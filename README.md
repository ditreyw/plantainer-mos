# Plantainer

Device for growbox atomization.

## Overview

1. Tested on `ESP32`
2. All code in `fs` folder and main file is `init.js`
3. Code divided by:
    1. `Modules` - independent physical module control system (like irrigation or light Modules)
    1. `Shadow layer` - Device logic of sharing state with Hub
    1. `Logger` - in `mlogging.js`

## How to install this app

1. Install and start [mos tool](https://mongoose-os.com/docs/quickstart/setup.md)
2. Switch to the Project page, build and flash it