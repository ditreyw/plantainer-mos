
function InitSocketTwo() {
    print('Init socketTwo START');

    let CfgGetSocketTwo = function (name) {
        return Cfg.get("socketTwo." + name);
    };

    let CfgGetSocketTwoState = function (name) {
        return CfgGetSocketTwo("state." + name);
    };

    let socketTwoId = CfgGetSocketTwo("id") || "socketTwo";

    let socketTwoState = {
        turnedOn: CfgGetSocketTwoState("turnedOn") || false,
        manualMode: CfgGetSocketTwoState("manualMode") || false,
        timers: {
            timerDayIntervals: {
                active: true,
                checkingInterval: 20000,
                restTimeTurnedOn: false,
                intervals: [
                    {
                        fromTimeHours: 1,
                        fromTimeMinutes: 0,
                        toTimeHours: 23,
                        toTimeMinutes: 59,
                        turnedOn: true,
                    }
                ]
            },
            timerEveryXHours: {
                active: false,
            },
            timerAt: {
                active: false,
                // callTimestamp: 0,
                // setTurnedOn: true,
                // lastCallTimestamp: 0,
            },
        }
    };

    let socketTwoSOptions = {
        id: socketTwoId,
        pin: 12,
        state: socketTwoState,
    };
    // GlobalObj[socketTwoId] = SmartSocket.create(socketTwoSOptions);
    GlobalObj.AddEntity(socketTwoId, SmartSocket.create(socketTwoSOptions));
    print('Init waterSocket ENDED');
}