

let VentilationModule = {
    id: 'ventilationModule',
    // coolerInPin: 5,
    // coolerOutPin: 5,
    timersIds: {
        // humidityCheckTimerId: null,
    },
    // state: {
    //     interval: 5000,
    //     coolerInTurnedOn: false,
    //     coolerOutTurnedOn: false,
    //     humidityMaxLvl: 50,
    //     humidityAverageLvl: 35,
    // },

    saveCfg: function(props) {
        let obj = {};
        obj[this.id] = props;
        Cfg.set(obj, true);
    },

    publishChangedState: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalUpdate(obj);
    },

    publishLocalReported: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalReported(obj);
    },

    SetTurnOnCoolers: function(value, publish) {
        this.state.coolerInTurnedOn = value;

        this.state.coolerOutTurnedOn = value;
        // this.saveCfg({
        //     state: {
        //         coolerInTurnedOn: value,
        //         coolerOutTurnedOn: value,
        //     }
        // })
        if (publish) {
            this.publishChangedState({
                coolerInTurnedOn: value,
                coolerOutTurnedOn: value,
            });
        }
    },

    activateMainInterval: function() {

        if (this.timersIds.humidityCheckTimerId) {
            Timer.del(this.timersIds.humidityCheckTimerId)
            this.timersIds.humidityCheckTimerId = null;
        }

        this.timersIds.humidityCheckTimerId = Timer.set(this.state.interval, true, function() {
            let hum = VentilationModule.HumiditySensor.GetHumidity();

            if (!hum) {
                print("Humidity get error!");
                return;
            }

            if (VentilationModule.state.mode === 'auto') {
                if (hum > VentilationModule.state.humidityMaxLvl) {
                    VentilationModule.SetTurnOnCoolers(true, false);
                }
                if (hum < VentilationModule.state.humidityAverageLvl) {
                    VentilationModule.SetTurnOnCoolers(false, false);
                }
            }

            VentilationModule.state.humidity = hum;
            
            // print("VentilationModule humidity");
            // print(hum);

            VentilationModule.publishChangedState({
                coolerInTurnedOn: VentilationModule.state.coolerInTurnedOn,
                coolerOutTurnedOn: VentilationModule.state.coolerOutTurnedOn,
            });
            VentilationModule.publishLocalReported({
                humidity: hum,
            });

        }, null)
    },

    HandleShadowDelta: function (newState, changedState) {
        if (newState.interval !== undefined) {
            if (newState.interval !== this.state.interval) {
                this.state.interval = newState.interval;
                // this.saveCfg({state: {interval: newState.interval}});
                changedState[this.id].interval = this.state.interval;
            }
        }
        if (newState.humidityMaxLvl !== undefined) {
            if (newState.humidityMaxLvl !== this.state.humidityMaxLvl) {
                this.state.humidityMaxLvl = newState.humidityMaxLvl;
                // this.saveCfg({state: {humidityMaxLvl: newState.humidityMaxLvl}});
                changedState[this.id].humidityMaxLvl = this.state.humidityMaxLvl;
            }
        }
        if (newState.humidityAverageLvl !== undefined) {
            if (newState.humidityAverageLvl !== this.state.humidityAverageLvl) {
                this.state.humidityAverageLvl = newState.humidityAverageLvl;
                // this.saveCfg({state: {humidityAverageLvl: newState.humidityAverageLvl}});
                changedState[this.id].humidityAverageLvl = this.state.humidityAverageLvl;
            }
        }
        if (newState.coolerInTurnedOn !== undefined) {
            if (newState.coolerInTurnedOn !== this.state.coolerInTurnedOn) {
                this.state.coolerInTurnedOn = newState.coolerInTurnedOn;
                GPIO.write(this.coolerInPin, !newState.coolerInTurnedOn);
                // this.saveCfg({state: {coolerInTurnedOn: newState.coolerInTurnedOn}});
                changedState[this.id].coolerInTurnedOn = this.state.coolerInTurnedOn;
            }
        }
        if (newState.coolerOutTurnedOn !== undefined) {
            if (newState.coolerOutTurnedOn !== this.state.coolerOutTurnedOn) {
                this.state.coolerOutTurnedOn = newState.coolerOutTurnedOn;
                GPIO.write(this.coolerOutPin, !newState.coolerOutTurnedOn);
                // this.saveCfg({state: {coolerOutTurnedOn: newState.coolerOutTurnedOn}});
                changedState[this.id].coolerOutTurnedOn = this.state.coolerOutTurnedOn;
            }
        }
        if (newState.mode !== undefined) {
            if (newState.mode !== this.state.mode) {
                this.state.mode = newState.mode;
                // this.saveCfg({state: {mode: newState.mode}});
                changedState[this.id].mode = this.state.mode;
            }
        }
        if (newState.mode !== undefined || newState.interval !== undefined || newState.coolerOutTurnedOn !== undefined || newState.coolerInTurnedOn !== undefined) {
            this.activateMainInterval();
        }
    },

    Init: function(options) {
        print("STARTED VentilationModule.Init");
        this.state = options.state;
        this.HumiditySensor = options.HumiditySensor;

        this.coolerInPin = options.coolerInPin;
        this.coolerOutPin = options.coolerOutPin;

        GPIO.set_mode(this.coolerInPin, GPIO.MODE_OUTPUT);
        GPIO.write(this.coolerInPin, !this.state.coolerInTurnedOn);

        GPIO.set_mode(this.coolerOutPin, GPIO.MODE_OUTPUT);
        GPIO.write(this.coolerOutPin, !this.state.coolerOutTurnedOn);

        this.activateMainInterval();

        print("ENDED VentilationModule.Init");
    }
}