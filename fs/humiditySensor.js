

let HumiditySensor = {
    create: function(options) {
        let obj = Object.create(this._proto);

        obj.dht = DHT.create(options.pin, DHT.DHT11);

        return obj;
    },

    _proto: {
        GetHumidity: function() {
            let val = this.dht.getHumidity();
            return val;
        },
        GetTemp: function() {
            return this.dht.getTemp();
        }
    }
}