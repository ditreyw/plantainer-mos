

let AirCompressorModule = {
    id: 'airCompressorModule',

    saveCfg: function(props) {
        let obj = {};
        obj[this.id] = props;
        Cfg.set(obj, true);
    },

    publishChangedState: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalUpdate(obj);
    },

    SetTurnedOn: function(value, publish) {
        this.state.turnedOn = value;
        this.saveCfg({
            state: {
                turnedOn: value,
            }
        })
        if (publish) {
            this.publishChangedState({
                turnedOn: value,
            });
        }
    },

    HandleShadowDelta: function (newState, changedState) {
        if (newState.turnedOn !== undefined) {
            if (newState.turnedOn !== this.state.turnedOn) {
                this.SetTurnedOn(newState.turnedOn, false);
                changedState[this.id].turnedOn = this.state.turnedOn;
            }
        }
    },

    Init: function(options){
        this.state = options.state;

        this.pin = options.pin;
        GPIO.set_mode(this.pin, GPIO.MODE_OUTPUT);
        GPIO.write(this.pin, !this.state.turnedOn);
    }
};