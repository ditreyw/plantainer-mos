

let MConditionService = {
    _conditions: {},
    AddCondition: function(key, fn) {
        if (this._conditions[key] === undefined) {
            this._conditions[key] = []
        }
        this._conditions[key].push(fn);
    },
    RemoveConditions: function(key, fn) {
        delete this._conditions[key]
    },
    CheckConditions: function(key, args) {
        let fns = this._conditions[key];
        for (let i = 0; i < fns.length; i++) {
            if (!fns[i](args)) {
                return false;
            }
        }
        return true;
    }
}