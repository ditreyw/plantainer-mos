


let LightModule = {
    id: 'lightModule',
    lightLvlCheckTimerId: null,
    lightIntervalTimerId: null,

    saveCfg: function(props) {
        // let obj = {};
        // obj[this.id] = props;
        // Cfg.set(obj, true);
    },

    publishChangedState: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalReported(obj);
    },

    resetTimers: function() {
        if (this.lightIntervalTimerId) {
            Timer.del(this.lightIntervalTimerId);
            this.lightIntervalTimerId = null;
        }
    },

    setPin: function(value) {
        this.lightSourcePin = value;
        GPIO.set_mode(this.lightSourcePin, GPIO.MODE_OUTPUT);
        this.saveCfg({lightSourcePin: value});
    },

    setMode: function (value, publish) {
        this.state.mode = value;
        this.saveCfg({ state: { mode: value } });
        if (publish) {
            this.publishChangedState({
                mode: value
            });
        }
    },

    setLightIntervalsArr: function(value, publish) {
        this.state.lightIntervalsArr = value;
        this.saveCfg({ state: { lightIntervalsArr: value } });
        if (publish) {
            this.publishChangedState({
                lightIntervalsArr: value
            });
        }
    },

    setLightLvlCheckActive: function (value, publish) {
        this.state.lightLvlCheckActive = value;
        this.saveCfg({ state: { lightLvlCheckActive: value } });
        if (publish) {
            this.publishChangedState({
                lightLvlCheckActive: value
            });
        }
    },

    setLightLvlCheckInterval: function (value, publish) {
        this.state.lightLvlCheckInterval = value;
        this.saveCfg({ state: { lightLvlCheckInterval: value } });
        if (publish) {
            this.publishChangedState({
                lightLvlCheckInterval: value
            });
        }
    },

    setLightIntervalsCheckingInterval: function (value, publish) {
        this.state.lightIntervalsCheckingInterval = value;
        this.saveCfg({ state: { lightIntervalsCheckingInterval: value } });
        if (publish) {
            this.publishChangedState({
                lightIntervalsCheckingInterval: value
            });
        }
    },

    SetTurnedOn: function (value, publish) {
        GPIO.write(this.lightSourcePin, !value);
        this.state.lightTurnedOn = value;
        this.saveCfg({ state: { lightTurnedOn: value } });
        if (publish) {
            this.publishChangedState({
                lightTurnedOn: value
            })
        }
    },

    GetLightLvl: function() {
        if (ADC.enable(this.lightSensorPin)) {
            let value = ADC.read(this.lightSensorPin);
            return 100 - value / 4095 * 100;
        } else {
            return 0
        }
    },

    SetLightLvl: function(value, publish) {
        this.state.lightLvl = value;
        if (publish) {
            this.publishChangedState({
                lightLvl: value
            })
        }
    },

    activateLightLvlCheckInterval: function() {
        if (!this.state.lightLvlCheckActive) {
            return;
        }

        if (this.lightLvlCheckTimerId) {
            Timer.del(this.lightLvlCheckTimerId);
            this.lightLvlCheckTimerId = null;
        }

        this.lightLvlCheckTimerId = Timer.set(this.state.lightLvlCheckInterval, true, function(){
            let lightLvl = LightModule.GetLightLvl();
            
            print("lightLvl: ");
            print(lightLvl);

            LightModule.SetLightLvl(lightLvl, false);
            
            // Send to server
            LightModule.publishChangedState({
                lightTurnedOn: LightModule.state.lightTurnedOn,
                lightLvl: lightLvl,
            });
        }, null);
    },

    activateLightIntervalsTimer: function() {
        if (this.state.mode !== "lightIntervalsTimerMode") {
            return;
        }

        let lightIntervalsArr = this.state.lightIntervalsArr;
        let now = Timer.now();
        let nowT = Timer.fmt("%R", now);
        let nowHour = JSON.parse(nowT.slice(0,2));
        let nowMin = JSON.parse(nowT.slice(3,5));
        let intervalActivated = false;

        for (let i = 0; i < lightIntervalsArr.length; i++) {
            let interval = lightIntervalsArr[i];
            if (
                nowHour >= interval.fromTimeHours && nowMin >= interval.fromTimeMinutes
                && nowHour <= interval.toTimeHours && nowMin < interval.toTimeMinutes
            ) {
                print("!!! intervalActivated !!!");
                intervalActivated = true;
                this.SetTurnedOn(interval.turnedOn);
                break;
            }
        }

        if (!intervalActivated) {
            this.SetTurnedOn(this.state.lightIntervalsRestTimeTurnedOn);
        }
        
        if (this.lightIntervalTimerId) {
          Timer.del(this.lightIntervalTimerId);
          this.lightIntervalTimerId = null;
        }

        print(this.state.lightIntervalsCheckingInterval);

        this.lightIntervalTimerId = Timer.set(this.state.lightIntervalsCheckingInterval, true, function () {
            LightModule.activateLightIntervalsTimer();
        }, null);
    },

    HandleShadowDelta: function (newState, changedState) {
        if (newState.lightTurnedOn !== undefined) {
            if (newState.lightTurnedOn !== this.state.lightTurnedOn) {
                // this.setMode("manual", false);
                this.SetTurnedOn(newState.lightTurnedOn, false);
            }
            changedState[this.id].lightTurnedOn = this.state.lightTurnedOn;
        }
        if (newState.mode !== undefined) {
            if (newState.mode !== this.state.mode) {
                this.setMode(newState.mode, false);
            }
            changedState[this.id].mode = this.state.mode;
        }
        if (newState.lightIntervalsArr !== undefined) {
            this.setLightIntervalsArr(newState.lightIntervalsArr, false);
            changedState[this.id].lightIntervalsArr = this.state.lightIntervalsArr;
        }
        if (newState.mode !== undefined || newState.lightIntervalsArr !== undefined) {
            this.resetTimers();
            if (this.state.mode === "lightIntervalsTimerMode") {
                this.activateLightIntervalsTimer();   
            }
        }
        if (newState.lightLvlCheckActive !== undefined) {
            if (newState.lightLvlCheckActive !== this.state.lightLvlCheckActive) {
                this.setLightLvlCheckActive(newState.lightLvlCheckActive, false);
                if (this.state.lightLvlCheckActive === false) {
                    if (this.lightLvlCheckTimerId) {
                        Timer.del(this.lightLvlCheckTimerId);
                        this.lightLvlCheckTimerId = null;
                    }
                }
            }
            changedState[this.id].lightLvlCheckActive = this.state.lightLvlCheckActive;
        }
        if (newState.lightLvlCheckInterval !== undefined) {
            if (newState.lightLvlCheckInterval !== this.state.lightLvlCheckInterval) {
                this.setLightLvlCheckInterval(newState.lightLvlCheckInterval, false);
            }
            changedState[this.id].lightLvlCheckInterval = this.state.lightLvlCheckInterval;
        }
        if (newState.lightLvlCheckInterval !== undefined || newState.lightLvlCheckActive !== undefined) {
            if (this.state.lightLvlCheckActive === true) {
                this.activateLightLvlCheckInterval();
            }
        }
        if (newState.lightIntervalsCheckingInterval !== undefined) {
            if (newState.lightIntervalsCheckingInterval !== this.state.lightIntervalsCheckingInterval) {
                this.setLightIntervalsCheckingInterval(newState.lightIntervalsCheckingInterval, false);
            }
            changedState[this.id].lightIntervalsCheckingInterval = this.state.lightIntervalsCheckingInterval;
        }
    },

    Init: function(options) {
        print("START LightModule.Init");
        if (options) {
            this.state = options.state;
        }

        // LightSource GPIO
        this.lightSourcePin = options.lightSourcePin;
        GPIO.set_mode(this.lightSourcePin, GPIO.MODE_OUTPUT);
        GPIO.write(this.lightSourcePin, !this.state.lightTurnedOn);

        // LightSensor GPIO
        this.lightSensorPin = options.lightSensorPin;
        print(ADC.enable(this.lightSensorPin));

        this.activateLightLvlCheckInterval();
        this.activateLightIntervalsTimer();
        
        print("END LightModule.Init");

        return true;
    },
}
