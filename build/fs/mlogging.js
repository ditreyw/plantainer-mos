let MLoggingService = {
  
    errorDev: function(deviceId, msg) {
      let newMsg = "[" + deviceId + "]["+JSON.stringify(Timer.now())+"][error]: " + msg;
      print(newMsg);
    },
  
    warnDev: function(deviceId, msg) {
      let newMsg = "[" + deviceId + "]["+JSON.stringify(Timer.now())+"][warn]: " + msg;
      print(newMsg);
    },
  
    infoDev: function(deviceId, msg) {
      let newMsg = "[" + deviceId + "]["+JSON.stringify(Timer.now())+"][info]: " + msg;
      print(newMsg);
    },
  
    debugDev: function(deviceId, msg) {
      let newMsg = "[" + deviceId + "]["+JSON.stringify(Timer.now())+"][debug]: " + msg;
      print(newMsg);
    },
  
    verboseDebugDev: function(deviceId, msg) {
      let newMsg = "[" + deviceId + "]["+JSON.stringify(Timer.now())+"][verboseDebug]: " + msg;
      print(newMsg);
    }
  };