

let MeteoStation = {
    // HumidityAndTempSensor: null,
    // interval: 6100,
    // state = {}
    id: 'meteoStation',

    saveCfg: function(props) {
        let obj = {};
        obj[this.id] = props;
        Cfg.set(obj, true);
    },

    publishChangedState: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalUpdate(obj);
    },

    activateMainInterval: function() {
        Timer.set(this.state.interval, true, function() {
            let hum = MeteoStation.HumiditySensor.GetHumidity();

            if (!hum) {
                print("Humidity get error!");
                return;
            }

            this.state.humidity = hum;

            let temp = MeteoStation.HumiditySensor.GetTemp();

            if (!temp) {
                print("Temp get error!");
                return;
            }

            this.state.tempereture = temp;

            MeteoStation.publishChangedState({
                humidity: hum,
                tempereture: temp,
            });
        }, null)
    },

    HandleShadowDelta: function (newState, changedState) {
        if (newState.interval !== undefined) {
            if (newState.interval !== this.state.interval) {
                this.state.interval = newState.interval;
                this.saveCfg({state: {interval: newState.interval}});
                changedState[this.id].interval = this.state.interval;
            }
        }
    },

    Init: function(options) {
        this.state = options.state;
        this.HumiditySensor = HumiditySensor.create({
            pin: options.pin,
        })
        this.activateMainInterval();
    }
};