load('api_config.js');
load('api_rpc.js');
load('api_gpio.js');
load('api_timer.js');
load('api_dht.js');
load('api_mqtt.js');
load('api_sys.js');
load("api_adc.js");

let deviceId = Cfg.get("device.id");

load('mcTimeService.js');
load('globalObj.js');
load('mshadow.js');

load('lightModule.js');
load('irrigationModule.js');
load('humiditySensor.js');
load('meteoStation.js');
load('ventilationModule.js');
load('airCompressorModule.js');

let containerHumSensor = HumiditySensor.create({
  pin: 4,
});

Timer.set(100, false, function() {
  LightModule.Init({
    id: 'lightModule',
    state: {
        mode: 'manual',
        lightTurnedOn: false,
        lightLvl: 0,
        lightLvlCheckActive: false,
        lightLvlCheckInterval: 5100,
        lightLvlCheckLastIntervalCallTimestamp: 0,
        lightIntervalsArr: [
            {
                fromTimeHours: 0,
                fromTimeMinutes: 0,
                toTimeHours: 0,
                toTimeMinutes: 44,
                turnedOn: true,
            }
        ],
        lightIntervalsRestTimeTurnedOn: false,
        lightIntervalsCheckingInterval: 20000,
    },
    lightSourcePin: 5,
    lightSensorPin: 32,
  });
  GlobalObj.AddEntity("lightModule", LightModule);
}, null);

Timer.set(200, false, function() {
  IrrigationModule.Init({
    HumiditySensor: containerHumSensor,
    id: 'irrigationModule',
    state: {
        mode: 'manual',
        irrigationTurnedOn: false,
        humidity: 0,

        // humidityCheck
        humidityCheckActive: false,
        humidityCheckInterval: 5000,
        humidityCheckLastIntervalCallTimestamp: 0,
        humidityCheckMinLvl: 20,
        humidityCheckAverageLvl: 25,
        humidityCheckMaxLvl: 50,

        // irrigationTimer
        irrigationTimerInProgress: false,
        irrigationTimerEveryXSeconds: 11000,
        irrigationTimerIrrigateYSeconds: 2000,
        irrigationTimerLastCallStartTimestamp: 0,
        irrigationTimerLastCallEndTimestamp: 0,
    },
    pumpPin: 19,
  });
  GlobalObj.AddEntity("irrigationModule", IrrigationModule);
}, null);

// Timer.set(300, false, function() {
//   MeteoStation.Init({
//     pin: 21,
//     state: {
//       interval: 6100,
//     }
//   });
//   GlobalObj.AddEntity("meteoStationModule", MeteoStation);
// }, null);

Timer.set(400, false, function() {
  AirCompressorModule.Init({
    state: {
      turnedOn: false,
    },
    pin: 22,
  });
  GlobalObj.AddEntity("airCompressorModule", AirCompressorModule);
}, null);

Timer.set(500, false, function() {
  VentilationModule.Init({
    HumiditySensor: containerHumSensor,
    coolerInPin: 23,
    coolerOutPin: 13,
    state: {
      interval: 20000,
      humidity: 0,
      coolerInTurnedOn: false,
      coolerOutTurnedOn: false,
      humidityMaxLvl: 50,
      humidityAverageLvl: 35,
      mode: "manual",
    },
  });
  GlobalObj.AddEntity("ventilationModule", VentilationModule);
  // GlobalObj.ready = true;
}, null);

Timer.set(1000, false, function() {
  MShadow.Init();
}, null);

print("!!! Initialized !!!");