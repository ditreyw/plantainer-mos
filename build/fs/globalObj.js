

let GlobalObj = {
    _entities: {},

    AddEntity: function (key, ent) {
        this._entities[key] = ent;
    },

    GetStates: function () {
        let state = {};
        for (let objName in this._entities) {
            if (this._entities[objName].state) state[objName] = this._entities[objName].state;
        }
        return state;
    },

    HandleShadowDelta: function (state, changedState) {
        for (let objName in this._entities) {
            if (state[objName] !== undefined) {
                changedState[objName] = {};
                this._entities[objName].HandleShadowDelta(state[objName], changedState);
                if (!MIsObjFilled(changedState[objName])) {
                    changedState[objName] = null;
                }
            }
        }
    }
}