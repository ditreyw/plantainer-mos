


let IrrigationModule = {
    id: 'irrigationModule',
    timersIds: {
        irrigationTimerId: null,
        humidityCheckTimerId: null,
    },

    // saveCfg: function(props) {
        // let obj = {};
        // obj[this.id] = props;
        // Cfg.set(obj, true);
    // },

    publishChangedState: function(changedState) {
        let obj = {};
        obj[this.id] = changedState;
        MShadow.PublishLocalUpdate(obj);
    },

    resetTimers: function() {
        if (this.timersIds.irrigationTimerId) {
            Timer.del(this.timersIds.irrigationTimerId);
        }
        if (this.timersIds.humidityCheckTimerId) {
            Timer.del(this.timersIds.humidityCheckTimerId);
        }
    },

    setPin: function(value) {
        this.pumpPin = value;
        GPIO.set_mode(this.pumpPin, GPIO.MODE_OUTPUT);
        // this.saveCfg({pumpPin: value});
    },

    setMode: function (value, publish) {
        this.state.mode = value;
        // this.saveCfg({ state: { mode: value } });
        if (publish) {
            this.publishChangedState({
                mode: value
            });
        }
        this.resetTimers();
        if (value === "irrigationTimerMode") {
            this.activateIrrigationTimer();   
        }
        // if (value === "humidityMode") {
            // this.activateIrrigationTimer();   
        // }
        // if (value === "manual") {
        //     this.resetTimers();
        // }
    },

    SetTurnedOn: function (value, publish) {
        GPIO.write(this.pumpPin, !value);
        this.state.irrigationTurnedOn = value;
        // this.saveCfg({ state: { irrigationTurnedOn: value } });
        if (publish) {
            this.publishChangedState({
                irrigationTurnedOn: value
            });
        }
    },

    GetHumidity: function() {
        return this.HumiditySensor.GetHumidity();
    },

    SetHumidity: function(value, publish) {
        this.state.humidity = value;
        if (publish) {
            this.publishChangedState({
                humidity: value
            });
        }
    },

    activateHumidityCheckInterval: function() {
        if (!this.state.humidityCheckActive) {
            return;
        }

        this.timersIds.humidityCheckTimerId = Timer.set(this.state.humidityCheckInterval, true, function(){
            let hum = IrrigationModule.GetHumidity();

            if (!hum) {
                print("Humidity get error!");
                return;
            }

            // print(hum);

            IrrigationModule.SetHumidity(hum, false);

            if (IrrigationModule.state.mode === 'humidityMode') {
                if (hum <= IrrigationModule.state.humidityCheckMinLvl && !IrrigationModule.state.irrigationTurnedOn) {
                    IrrigationModule.SetTurnedOn(true, false);
                }
                if (hum > IrrigationModule.state.humidityCheckAverageLvl && IrrigationModule.state.irrigationTurnedOn) {
                    IrrigationModule.SetTurnedOn(false, false);
                }
            }

            let now = Timer.now();
            
            // Send to server
            IrrigationModule.publishChangedState({
                irrigationTurnedOn: IrrigationModule.state.irrigationTurnedOn,
                humidity: hum,
                humidityCheckLastIntervalCallTimestamp: now
            });

            // IrrigationModule.saveCfg({
            //     state: {
            //         humidityCheckLastIntervalCallTimestamp: now
            //     }
            // });
        }, null);
    },

    runIrrigationTimerCB: function() {
        print("runIrrigationTimerCB START");

        if (IrrigationModule.state.mode !== 'irrigationTimerMode') {
            return;
        }

        let now = Timer.now();

        IrrigationModule.state.irrigationTimerInProgress = false;
        IrrigationModule.SetTurnedOn(false, false);
        IrrigationModule.state.irrigationTimerLastCallEndTimestamp = now;

        // IrrigationModule.saveCfg({
        //     state: {
        //         irrigationTimerInProgress: false,
        //         irrigationTimerLastCallEndTimestamp: now,
        //     }
        // });

        IrrigationModule.publishChangedState({
            irrigationTimerInProgress: false,
            irrigationTimerLastCallEndTimestamp: now,
            irrigationTurnedOn: false,
        });

        IrrigationModule.activateIrrigationTimer();

        print("runIrrigationTimerCB END");
    },

    runIrrigationTimer: function() {
        print("runIrrigationTimer START");

        if (this.state.mode !== 'deviceIrrigationTimerMode') {
            return;
        }

        if (this.state.irrigationTimerInProgress) {
            return;
        }

        let now = Timer.now();

        this.state.irrigationTimerInProgress = true;
        this.SetTurnedOn(true, false);
        this.state.irrigationTimerLastCallStartTimestamp = now;

        // this.saveCfg({
        //     state: {
        //         irrigationTimerInProgress: true,
        //         irrigationTimerLastCallStartTimestamp: now,
        //     }
        // });

        this.publishChangedState({
            irrigationTimerInProgress: true,
            irrigationTimerLastCallStartTimestamp: now,
            irrigationTurnedOn: true,
        });

        this.timersIds.irrigationTimerId = Timer.set(this.state.irrigationTimerIrrigateYSeconds, false, this.runIrrigationTimerCB, null);
        print("runIrrigationTimer END");
    },

    activateIrrigationTimer: function() {
        if (this.state.mode === 'irrigationTimerMode') {
            if (this.state.irrigationTimerLastCallEndTimestamp === 0) {
                this.runIrrigationTimer();
            } else {
                let when = (this.state.irrigationTimerLastCallEndTimestamp + this.state.irrigationTimerEveryXSeconds) - Timer.now();
                if (when > 0) {
                    this.timersIds.irrigationTimerId = Timer.set(when, false, function () {
                        IrrigationModule.runIrrigationTimer();
                    }, null);
                } else {
                    this.runIrrigationTimer();
                }
            }
        }
    },

    HandleShadowDelta: function (newState, changedState) {
        if (newState.irrigationTurnedOn !== undefined) {
            if (newState.irrigationTurnedOn !== this.state.irrigationTurnedOn) {
                // this.setMode("manual", false);
                this.SetTurnedOn(newState.irrigationTurnedOn, false);
                changedState[this.id].irrigationTurnedOn = this.state.irrigationTurnedOn;
            }
        }
        if (newState.mode !== undefined) {
            if (newState.mode !== this.state.mode) {
                this.setMode(newState.mode, false);
                changedState[this.id].mode = this.state.mode;
            }
        }
        // let nnState = {};
        if (newState.humidityCheckActive !== undefined) {
            if (newState.humidityCheckActive !== this.state.humidityCheckActive) {
                this.state.humidityCheckActive = newState.humidityCheckActive;
                // nnState.humidityCheckActive = newState.humidityCheckActive;
                changedState[this.id].humidityCheckActive = this.state.humidityCheckActive;
                if (newState.humidityCheckActive) {
                    this.activateHumidityCheckInterval();
                }
            }
        }
        if (newState.humidityCheckInterval !== undefined) {
            if (newState.humidityCheckInterval !== this.state.humidityCheckInterval) {
                this.state.humidityCheckInterval = newState.humidityCheckInterval;
                // nnState.humidityCheckInterval = newState.humidityCheckInterval;
                changedState[this.id].humidityCheckInterval = this.state.humidityCheckInterval;
            }
        }
        if (newState.humidityCheckMinLvl !== undefined) {
            if (newState.humidityCheckMinLvl !== this.state.humidityCheckMinLvl) {
                this.state.humidityCheckMinLvl = newState.humidityCheckMinLvl;
                // nnState.humidityCheckMinLvl = newState.humidityCheckMinLvl;
                changedState[this.id].humidityCheckMinLvl = this.state.humidityCheckMinLvl;
            }
        }
        if (newState.humidityCheckAverageLvl !== undefined) {
            if (newState.humidityCheckAverageLvl !== this.state.humidityCheckAverageLvl) {
                this.state.humidityCheckAverageLvl = newState.humidityCheckAverageLvl;
                // nnState.humidityCheckAverageLvl = newState.humidityCheckAverageLvl;
                changedState[this.id].humidityCheckAverageLvl = this.state.humidityCheckAverageLvl;
            }
        }
        if (newState.humidityCheckMaxLvl !== undefined) {
            if (newState.humidityCheckMaxLvl !== this.state.humidityCheckMaxLvl) {
                this.state.humidityCheckMaxLvl = newState.humidityCheckMaxLvl;
                // nnState.humidityCheckMaxLvl = newState.humidityCheckMaxLvl;
                changedState[this.id].humidityCheckMaxLvl = this.state.humidityCheckMaxLvl;
            }
        }
        if (newState.irrigationTimerEveryXSeconds !== undefined) {
            if (newState.irrigationTimerEveryXSeconds !== this.state.irrigationTimerEveryXSeconds) {
                this.state.irrigationTimerEveryXSeconds = newState.irrigationTimerEveryXSeconds;
                // nnState.irrigationTimerEveryXSeconds = newState.irrigationTimerEveryXSeconds;
                changedState[this.id].irrigationTimerEveryXSeconds = this.state.irrigationTimerEveryXSeconds;
            }
        }
        if (newState.irrigationTimerIrrigateYSeconds !== undefined) {
            if (newState.irrigationTimerIrrigateYSeconds !== this.state.irrigationTimerIrrigateYSeconds) {
                this.state.irrigationTimerIrrigateYSeconds = newState.irrigationTimerIrrigateYSeconds;
                // nnState.irrigationTimerIrrigateYSeconds = newState.irrigationTimerIrrigateYSeconds;
                changedState[this.id].irrigationTimerIrrigateYSeconds = this.state.irrigationTimerIrrigateYSeconds;
            }
        }
        // this.saveCfg({state: nnState});
    },

    Init: function(options) {
        print("START IrrigationModule.Init");
        if (options) {
            this.state = options.state;
        }

        this.HumiditySensor = options.HumiditySensor;

        this.setPin(options.pumpPin);
        this.SetTurnedOn(options.state.irrigationTurnedOn || false, false);
        this.activateHumidityCheckInterval();
        this.activateIrrigationTimer();

        print("END IrrigationModule.Init");
        
        return true;
    },
}
