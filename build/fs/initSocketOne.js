function InitSocketOne() {
    print('Init SocketOne START');
    let CfgGetSocketOne = function (name) {
        return Cfg.get("socketOne." + name);
    };

    let CfgGetSocketOneState = function (name) {
        return CfgGetSocketOne("state." + name);
    };

    let socketOneId = CfgGetSocketOne("id");

    let SSState = {
        turnedOn: CfgGetSocketOneState("turnedOn"),
        manualMode: CfgGetSocketOneState("manualMode"),
        timers: {
            timerEveryXHours: {
                active: CfgGetSocketOneState("timers.timerEveryXHours.active"),
                inProgress: false,
                everyXSeconds: CfgGetSocketOneState("timers.timerEveryXHours.everyXSeconds"),
                intervalInSec: CfgGetSocketOneState("timers.timerEveryXHours.intervalInSec"),
                lastCallStartTimestamp: CfgGetSocketOneState("timers.timerEveryXHours.lastCallStartTimestamp"),
                lastCallEndTimestamp: CfgGetSocketOneState("timers.timerEveryXHours.lastCallEndTimestamp"),
                setTurnedOnOnStart: CfgGetSocketOneState("timers.timerEveryXHours.setTurnedOnOnStart"),
                setTurnedOnOnEnd: CfgGetSocketOneState("timers.timerEveryXHours.setTurnedOnOnEnd"),
            },
            timerDayIntervals: {
                active: false,
            },
            timerAt: {
                active: false,
                // callTimestamp: 0,
                // setTurnedOn: true,
                // lastCallTimestamp: 0,
            },
        }
    };
    let ssOptions = {
        id: socketOneId,
        pin: CfgGetSocketOne("pin"),
        state: SSState,
    };
    // GlobalObj[socketOneId] = SmartSocket.create(ssOptions);
    GlobalObj.AddEntity(socketOneId, SmartSocket.create(ssOptions));
    print('Init SocketOne ENDED');
}