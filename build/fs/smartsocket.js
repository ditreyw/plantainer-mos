

let SmartSocket = {
    create: function (options) {
        let obj = Object.create(this._proto);

        // // MAIN
        obj.id = options.id;
        obj.state = options.state;
        obj.timersIds = {
            // mainTimerId: 0,
        };

        // // GPIO
        obj.pin = options.pin;
        GPIO.set_mode(obj.pin, GPIO.MODE_OUTPUT);

        // // STATE
        obj.setTurnedOn(options.state.turnedOn || false);
        obj._activateMainTimer();

        return obj;
    },

    _proto: {
        publishChangedState: function (changedState) {
            let obj = {};
            obj[this.id] = changedState;
            // MShadow.PublishLocalUpdate(obj);
        },

        _runTimerCbEveryXHours: function (obj) {
            if (obj.state.manualMode) {
                return;
            }

            let timerEXHO = obj.state.timers.timerEveryXHours;
            obj.setTurnedOn(timerEXHO.setTurnedOnOnEnd);
            let now = Timer.now();
            timerEXHO.lastCallEndTimestamp = now;
            timerEXHO.inProgress = false;
            obj.publishChangedState({
                timers: {
                    timerEveryXHours: {
                        lastCallEndTimestamp: now,
                        inProgress: false
                    }
                },
                turnedOn: timerEXHO.setTurnedOnOnEnd
            });

            let localSaveObj = {};
            localSaveObj[obj.id] = {
                state: {
                    timers: {
                        timerEveryXHours: {
                            lastCallEndTimestamp: now
                        }
                    }
                }
            };
            // Cfg.set(localSaveObj, true);

            obj._activateMainTimer();
            print("_runTimerEveryXHours ENDED");
        },

        _runTimerEveryXHours: function () {
          print("_runTimerEveryXHours START");
            if (this.state.manualMode) {
                return;
            }

            // ToDo: Add MConditionsService

            let tEXHO = this.state.timers.timerEveryXHours;
            if (tEXHO.inProgress) {
                return;
            }

            this.setTurnedOn(tEXHO.setTurnedOnOnStart);
            tEXHO.inProgress = true;

            let now = Timer.now();
            tEXHO.lastCallStartTimestamp = now;

            let saveObj = {};
            saveObj[this.id] = {
                state: {
                    timers: {
                        // timerEveryXHours: {
                        // lastCallStartTimestamp: now,
                        // }
                    }
                }
            };
            // Cfg.set(saveObj, true);

            // TODO: send to server lastCallStartTimestamp + inProgress + turnedOn
            this.publishChangedState({
                timers: {
                    timerEveryXHours: {
                        lastCallStartTimestamp: now,
                        inProgress: true
                    }
                },
                turnedOn: tEXHO.setTurnedOnOnStart,
            });

            Timer.set(tEXHO.intervalInSec, false, this._runTimerCbEveryXHours, this);
        },

        _runTimerAt: function () {
            if (this.state.manualMode) {
                return;
            }

            // ToDo: Add MConditionsService

            this.setTurnedOn(this.state.timers.timerAt.setTurnedOn, true);
            this.state.timers.timerAt.lastCallTimestamp = Timers.now();
            // TODO: save to CFG
        },

        _activateMainTimer: function () {
            let timers = this.state.timers;
            if (timers === undefined) {
                return false;
            }

            if (timers.timerEveryXHours.active && timers.timerAt.active) {
                print("Error both timers are active!");
            }

            if (timers.timerEveryXHours.active) {
                let timerEveryXHours = timers.timerEveryXHours;
                let when = (timerEveryXHours.lastCallEndTimestamp + timerEveryXHours.everyXSeconds) - Timer.now();
                print(when);
                if (when > 0) {
                    this.timersIds.mainTimerId = Timer.set(when, false, function (ud) {
                      ud._runTimerEveryXHours();
                    }, this);
                } else {
                    this._runTimerEveryXHours();
                }
            } else if (timers.timerDayIntervals.active) {
                let timerDayIntervals = timers.timerDayIntervals;
                let now = Timer.now();
                let nowT = Timer.fmt("%R", now);
                let nowHour = +nowT.slice(0,2);
                let nowMin = +nowT.slice(3,5);
                let intervalIsRunning = false;

                print("timerDayIntervals");
                print(nowT);
                print(now);
                print(nowHour);
                print(nowMin);

                for (let i = 0; i < timerDayIntervals.intervals.length; i++) {
                    let interval = timerDayIntervals.intervals[i];
                    if (
                        nowHour >= interval.fromTimeHours && nowMin >= interval.fromTimeMinutes
                        && nowHour < interval.toTimeHours && nowMin < interval.toTimeMinutes
                    ) {
                        print("!!! intervalIsRunning");
                        intervalIsRunning = true;
                        this.setTurnedOn(interval.turnedOn);
                        break;
                    }
                }

                if (!intervalIsRunning) {
                    this.setTurnedOn(timerDayIntervals.restTimeTurnedOn);
                }
                
                print(timerDayIntervals.checkingInterval);
                
                if (this.timersIds.mainTimerId) {
                  Timer.del(this.timersIds.mainTimerId);
                }

                this.timersIds.mainTimerId = Timer.set(timerDayIntervals.checkingInterval, true, function (ud) {
                    ud._activateMainTimer();
                }, this);
            } else if (timers.timerAt.active) {
                let timerAt = timers.timerAt;
                let when = timerAt.callTimestamp - Timer.now();
                if (when > 0) {
                    this.timersIds.mainTimerId = Timer.set(when, false, function (ud) {
                        ud._runTimerAt();
                    }, this);
                } else {
                    this._runTimerAt();
                }
            }
        },

        setManualMode: function (value, publish) {
            this.state.manualMode = value;
            let obj = {};
            obj[this.id] = { state: { manualMode: this.state.manualMode } };
            Cfg.set(obj, true);
            if (publish) {
                this.publishChangedState({
                    manualMode: value
                });
            }
        },

        setTurnedOn: function (value, publish) {
            GPIO.write(this.pin, !value);
            this.state.turnedOn = value;
            let obj = {};
            obj[this.id] = { state: { turnedOn: value } };
            Cfg.set(obj, true);
            if (publish) {
                this.publishChangedState({
                    turnedOn: value
                })
            }
        },

        _resetTimers: function () {
            this.state.timers.timerEveryXHours.active = false;
            this.state.timers.timerAt.active = false;
            if (this.timersIds.mainTimerId !== undefined) {
                Timer.del(this.timersIds.mainTimerId);
            }
        },

        setTimerEveryXHours: function (options, publish) {
            let timerEveryXHours = this.state.timers.timerEveryXHours;

            let saveObj = {};
            saveObj[this.id] = {
                state: {
                    timers: {
                        timerEveryXHours: {}
                    }
                }
            };

            let saveEvery = saveObj[this.id].state.timers.timerEveryXHours;

            if (options.active) {
                this._resetTimers();
                this.setManualMode(false);
            }
            if (options.active !== undefined) {
                timerEveryXHours.active = options.active;
                saveEvery.active = options.active;
            }
            if (options.everyXSeconds !== undefined) {
                timerEveryXHours.everyXSeconds = options.everyXSeconds;
                saveEvery.everyXSeconds = options.everyXSeconds;
            }
            if (options.intervalInSec !== undefined) {
                timerEveryXHours.intervalInSec = options.intervalInSec;
                saveEvery.intervalInSec = options.intervalInSec;
            }
            if (options.setTurnedOnOnStart !== undefined) {
                timerEveryXHours.setTurnedOnOnStart = options.setTurnedOnOnStart;
                saveEvery.setTurnedOnOnStart = options.setTurnedOnOnStart;
            }
            if (options.setTurnedOnOnEnd !== undefined) {
                timerEveryXHours.setTurnedOnOnEnd = options.setTurnedOnOnEnd;
                saveEvery.setTurnedOnOnEnd = options.setTurnedOnOnEnd;
            }

            Cfg.set(saveObj, true);

            if (publish) {
                this.publishChangedState({
                    timers: {
                        timerEveryXHours: saveEvery
                    }
                })
            }
        },

        setTimerAt: function (options, publish) {
            if (options.active) {
                this.setManualMode(false);
                this._resetTimers();
            }
            this.state.timers.timerAt = options;
            if (publish) {
                this.publishChangedState({
                    timers: {
                        timerAt: options
                    }
                })
            }
        },

        setTimerDayIntervals: function(options) {
            let timerDayIntervals = this.state.timers.timerDayIntervals;

            let saveObj = {};
            saveObj[this.id] = {
                state: {
                    timers: {
                        timerDayIntervals: {}
                    }
                }
            };

            let saveEvery = saveObj[this.id].state.timers.timerDayIntervals;

            if (options.active) {
                this._resetTimers();
                this.setManualMode(false);
            }
            if (options.active !== undefined) {
                timerDayIntervals.active = options.active;
                saveEvery.active = options.active;
            }
            if (options.restTimeTurnedOn !== undefined) {
                timerDayIntervals.restTimeTurnedOn = options.restTimeTurnedOn;
                saveEvery.restTimeTurnedOn = options.restTimeTurnedOn;
            }
            if (options.checkingInterval !== undefined) {
                timerDayIntervals.checkingInterval = options.checkingInterval;
                saveEvery.checkingInterval = options.checkingInterval;
            }
            if (options.intervals !== undefined) {
                timerDayIntervals.intervals = options.intervals;
                saveEvery.intervals = options.intervals;
            }

            Cfg.set(saveObj, true);

            if (publish) {
                this.publishChangedState({
                    timers: {
                        timerDayIntervals: saveEvery
                    }
                })
            }
        },

        setTimers: function (timers, publish) {
            if (timers === undefined) {
                return false
            }

            if (timers.timerEveryXHours !== undefined) {
                this.setTimerEveryXHours(timers.timerEveryXHours, publish)
            }

            if (timers.timerAt !== undefined) {
                this.setTimerAt(timers.timerAt, publish)
            }

            if (timers.timerDayIntervals !== undefined) {
                this.setTimerDayIntervals(timers.timerDayIntervals, publish)
            }

            this._activateMainTimer();
        },

        handleShadowDelta: function (newState, changedState) {
            if (newState.turnedOn !== undefined) {
                if (newState.turnedOn !== this.state.turnedOn) {
                    this.setTurnedOn(newState.turnedOn, false);
                    changedState[this.id].turnedOn = this.state.turnedOn;
                }
            }
            if (newState.manualMode !== undefined) {
                if (newState.manualMode !== this.state.manualMode) {
                    this.setManualMode(newState.manualMode, false);
                    changedState[this.id].manualMode = this.state.manualMode;
                }
            }
            if (newState.timers !== undefined) {
                this.setTimers(newState.timers, false);
                changedState[this.id].timers = this.state.timers;
            }
        }
    }
}