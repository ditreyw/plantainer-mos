

let MShadow = {
    _serverTopicName: Cfg.get('mcserver.topicName'),
    _deviceId: Cfg.get('device.id'),
    _deviceType: Cfg.get('device.id'),
    _deviceRPCPrefix: Cfg.get('device.rpcPrefix'),
    _serverId: Cfg.get('mcserver.id'),

    _version: 0,

    _setCurrentVersion: function(version) {
        this._version = version;
    },

    GetCurrentVersion: function() {
        return this._version;
    },

    checkAndSetVersion: function(version){
        print("CheckAndSetVersion");
        print(this.GetCurrentVersion());
        print(version);
        if (version === undefined || version < this.GetCurrentVersion()) {
          return false;
        }
        this._setCurrentVersion(version);
        return true;
    },
    
    PublishLocalReported: function(changedProps) {
      let updatedState = {
        state: {
          reported: changedProps,
        },
        timestamp: this.GetFmtTimestamp(),
        version: this.GetCurrentVersion()
      };

      let rpc = {
        "id": Math.floor(Math.random() * 100000),
        "src": this._deviceId,
        "dst": this._serverId,
        "method": this._deviceRPCPrefix + ".Shadow.Update",
        "args": updatedState,
      };

      MQTT.pub(this._serverTopicName + "/rpc", JSON.stringify(rpc), 1);
      return true;
    },

    PublishLocalUpdate: function(changedProps) {
      let updatedState = {
        state: {
          reported: changedProps,
          desired: changedProps,
        },
        timestamp: this.GetFmtTimestamp(),
        version: this.GetCurrentVersion()
      };

      let rpc = {
        "id": Math.floor(Math.random() * 100000),
        "src": this._deviceId,
        "dst": this._serverId,
        "method": this._deviceRPCPrefix + ".Shadow.Update",
        "args": updatedState,
      };
      
      // print(JSON.stringify(rpc));

      MQTT.pub(this._serverTopicName + "/rpc", JSON.stringify(rpc), 1);
      return true;

      // WORKING
      // return RPC.call(this._serverTopicName, this._deviceRPCPrefix + ".Shadow.Update", updatedState, function(resp, err_code, err_msg, ud) {
      //   // print(JSON.stringify(resp));
      //   if (err_code) {
      //     print(err_code);
      //     return true;
      //   } else {
      //     MShadow.checkAndSetVersion(resp.result.version);
      //     return true;
      //   }
      // }, null);

      // ______
  
      // let rpc = {
      //   src: TZShadow.DeviceId,
      //   dst: TZShadow._serverTopicName+"/rpc",
      //   method: TZShadow.DeviceId + '.Shadow.Update',
      //   args: updatedState,
      // };
      //
      // // RESPONSE to /:server_id/:shadow_id/update {"reported": ..., "desired": ...}
      // MQTT.pub(this._serverTopicName+"/rpc", JSON.stringify(response), 1);
  
      // return true;
    },

    GetFmtTimestamp: function() {
        return Timer.fmt("%Y-%m-%dT%H:%M:%SZ", McTimeService.now());
    },

    ShadowDeltaCb: function(args, changedState) {
        let state = args.state;
        if (state !== undefined) {
          GlobalObj.HandleShadowDelta(state, changedState);
        } else {
          return false
        }
    },
    
    // connectedReportTimerId: null,

    _connectedReport: function() {
      // print("!!! MShadow _connectedReport try");
      // if (this.connectedReportTimerId) {
      //   Timer.del(this.connectedReportTimerId);
      //   this.connectedReportTimerId = null;
      // }
      // if (!GlobalObj.ready) {
      //   this.connectedReportTimerId = Timer.set(500, false, function() {
      //     MShadow._connectedReport();
      //   }, null);
      //   return;
      // }
      print("!!! MShadow _connectedReport start");
      let updatedState = {
        state: {
          reported: GlobalObj.GetStates(),
        },
        timestamp: this.GetFmtTimestamp(),
        version: this.GetCurrentVersion()
      };
      let rpc = {
        "id": Math.floor(Math.random() * 100000),
        "src": this._deviceId,
        "dst": this._serverId,
        "method": this._deviceRPCPrefix + ".Shadow.Update",
        "args": updatedState,
      };
      return MQTT.pub(this._serverTopicName + "/rpc", JSON.stringify(rpc), 1);
    },

    Init: function() {
      RPC.addHandler(this._deviceRPCPrefix + '.Shadow.Get', function(args, sm) {
          return {
            version: MShadow.GetCurrentVersion(),
            timestamp: MShadow.GetFmtTimestamp(),
            state: GlobalObj.GetStates(),
          };
      }, null);

      RPC.addHandler(this._deviceRPCPrefix + '.Shadow.Delta', function(args, sm, obj) {
        if (args.version === undefined || args.version < obj.GetCurrentVersion()) {
          return {
            error: {
              code: 409,
              msg: "Wrong version"
            }
          };
        }
      
        let state = {};
      
        obj.ShadowDeltaCb(args, state);
      
        let empty = true;
        for(let k in state) {
          empty = false;
        }
        if (empty) return true;
      
        obj._setCurrentVersion(args.version);
      
        let updatedState = {
          state: {
            reported: state,
          },
          // Check if timestamp is not already in RPC
          timestamp: obj.GetFmtTimestamp(),
          version: args.version
        };

        let rpc = {
          "id": Math.floor(Math.random() * 100000),
          "src": obj._deviceId,
          "dst": obj._serverId,
          "method": obj._deviceRPCPrefix + ".Shadow.Update",
          "args": updatedState,
        };
        MQTT.pub(obj._serverTopicName + "/rpc", JSON.stringify(rpc), 1);
      
        // RESPONSE to :server_id/rpc {"result": {"state": ...}, "id": 100}
        return updatedState;
      }, this);

      RPC.addHandler(this._deviceRPCPrefix + '.Shadow.Update.Accepted', function(args, sm, obj) {
        MShadow.checkAndSetVersion(args.version || args.metadata && args.metadata.version);
        return true;
      }, null);
  
      RPC.addHandler(this._deviceRPCPrefix + '.Shadow.Get.Accepted', function(args, sm, obj) {
        MShadow.checkAndSetVersion(args.version || args.metadata && args.metadata.version);
        return true;
      }, null);

      MShadow._connectedReport();

      MQTT.setEventHandler(function(conn, ev, edata) {
        if (ev === MQTT.EV_SUBACK) {
          // MShadow._connectedReport();
        } else if (ev === MQTT.EV_CONNACK) {
          print("!!! MQTT.EV_CONNACK");
          // print(JSON.stringify(edata));
          MShadow._connectedReport();
        }
      }, null);

    }
};