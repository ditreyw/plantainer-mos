/* Generated file - do not edit. */

#include <stddef.h>
#include "mgos_config.h"

const struct mgos_conf_entry mgos_config_schema_[186] = {
  {.type = CONF_TYPE_OBJECT, .key = "", .num_desc = 185},
  {.type = CONF_TYPE_OBJECT, .key = "update", .num_desc = 8},
  {.type = CONF_TYPE_INT, .key = "timeout", .offset = offsetof(struct mgos_config, update.timeout)},
  {.type = CONF_TYPE_INT, .key = "commit_timeout", .offset = offsetof(struct mgos_config, update.commit_timeout)},
  {.type = CONF_TYPE_STRING, .key = "url", .offset = offsetof(struct mgos_config, update.url)},
  {.type = CONF_TYPE_INT, .key = "interval", .offset = offsetof(struct mgos_config, update.interval)},
  {.type = CONF_TYPE_STRING, .key = "ssl_ca_file", .offset = offsetof(struct mgos_config, update.ssl_ca_file)},
  {.type = CONF_TYPE_STRING, .key = "ssl_client_cert_file", .offset = offsetof(struct mgos_config, update.ssl_client_cert_file)},
  {.type = CONF_TYPE_STRING, .key = "ssl_server_name", .offset = offsetof(struct mgos_config, update.ssl_server_name)},
  {.type = CONF_TYPE_BOOL, .key = "enable_post", .offset = offsetof(struct mgos_config, update.enable_post)},
  {.type = CONF_TYPE_OBJECT, .key = "device", .num_desc = 4},
  {.type = CONF_TYPE_STRING, .key = "id", .offset = offsetof(struct mgos_config, device.id)},
  {.type = CONF_TYPE_STRING, .key = "password", .offset = offsetof(struct mgos_config, device.password)},
  {.type = CONF_TYPE_STRING, .key = "type", .offset = offsetof(struct mgos_config, device.type)},
  {.type = CONF_TYPE_STRING, .key = "rpcPrefix", .offset = offsetof(struct mgos_config, device.rpcPrefix)},
  {.type = CONF_TYPE_OBJECT, .key = "debug", .num_desc = 10},
  {.type = CONF_TYPE_STRING, .key = "udp_log_addr", .offset = offsetof(struct mgos_config, debug.udp_log_addr)},
  {.type = CONF_TYPE_INT, .key = "level", .offset = offsetof(struct mgos_config, debug.level)},
  {.type = CONF_TYPE_STRING, .key = "filter", .offset = offsetof(struct mgos_config, debug.filter)},
  {.type = CONF_TYPE_INT, .key = "stdout_uart", .offset = offsetof(struct mgos_config, debug.stdout_uart)},
  {.type = CONF_TYPE_INT, .key = "stderr_uart", .offset = offsetof(struct mgos_config, debug.stderr_uart)},
  {.type = CONF_TYPE_INT, .key = "factory_reset_gpio", .offset = offsetof(struct mgos_config, debug.factory_reset_gpio)},
  {.type = CONF_TYPE_STRING, .key = "mg_mgr_hexdump_file", .offset = offsetof(struct mgos_config, debug.mg_mgr_hexdump_file)},
  {.type = CONF_TYPE_INT, .key = "mbedtls_level", .offset = offsetof(struct mgos_config, debug.mbedtls_level)},
  {.type = CONF_TYPE_STRING, .key = "stdout_topic", .offset = offsetof(struct mgos_config, debug.stdout_topic)},
  {.type = CONF_TYPE_STRING, .key = "stderr_topic", .offset = offsetof(struct mgos_config, debug.stderr_topic)},
  {.type = CONF_TYPE_OBJECT, .key = "sys", .num_desc = 10},
  {.type = CONF_TYPE_OBJECT, .key = "mount", .num_desc = 5},
  {.type = CONF_TYPE_STRING, .key = "path", .offset = offsetof(struct mgos_config, sys.mount.path)},
  {.type = CONF_TYPE_STRING, .key = "dev_type", .offset = offsetof(struct mgos_config, sys.mount.dev_type)},
  {.type = CONF_TYPE_STRING, .key = "dev_opts", .offset = offsetof(struct mgos_config, sys.mount.dev_opts)},
  {.type = CONF_TYPE_STRING, .key = "fs_type", .offset = offsetof(struct mgos_config, sys.mount.fs_type)},
  {.type = CONF_TYPE_STRING, .key = "fs_opts", .offset = offsetof(struct mgos_config, sys.mount.fs_opts)},
  {.type = CONF_TYPE_STRING, .key = "tz_spec", .offset = offsetof(struct mgos_config, sys.tz_spec)},
  {.type = CONF_TYPE_INT, .key = "wdt_timeout", .offset = offsetof(struct mgos_config, sys.wdt_timeout)},
  {.type = CONF_TYPE_STRING, .key = "pref_ota_lib", .offset = offsetof(struct mgos_config, sys.pref_ota_lib)},
  {.type = CONF_TYPE_INT, .key = "esp32_adc_vref", .offset = offsetof(struct mgos_config, sys.esp32_adc_vref)},
  {.type = CONF_TYPE_STRING, .key = "conf_acl", .offset = offsetof(struct mgos_config, conf_acl)},
  {.type = CONF_TYPE_OBJECT, .key = "bt", .num_desc = 13},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, bt.enable)},
  {.type = CONF_TYPE_STRING, .key = "dev_name", .offset = offsetof(struct mgos_config, bt.dev_name)},
  {.type = CONF_TYPE_BOOL, .key = "adv_enable", .offset = offsetof(struct mgos_config, bt.adv_enable)},
  {.type = CONF_TYPE_STRING, .key = "scan_rsp_data_hex", .offset = offsetof(struct mgos_config, bt.scan_rsp_data_hex)},
  {.type = CONF_TYPE_BOOL, .key = "keep_enabled", .offset = offsetof(struct mgos_config, bt.keep_enabled)},
  {.type = CONF_TYPE_BOOL, .key = "allow_pairing", .offset = offsetof(struct mgos_config, bt.allow_pairing)},
  {.type = CONF_TYPE_INT, .key = "max_paired_devices", .offset = offsetof(struct mgos_config, bt.max_paired_devices)},
  {.type = CONF_TYPE_BOOL, .key = "random_address", .offset = offsetof(struct mgos_config, bt.random_address)},
  {.type = CONF_TYPE_OBJECT, .key = "gatts", .num_desc = 2},
  {.type = CONF_TYPE_INT, .key = "min_sec_level", .offset = offsetof(struct mgos_config, bt.gatts.min_sec_level)},
  {.type = CONF_TYPE_BOOL, .key = "require_pairing", .offset = offsetof(struct mgos_config, bt.gatts.require_pairing)},
  {.type = CONF_TYPE_BOOL, .key = "config_svc_enable", .offset = offsetof(struct mgos_config, bt.config_svc_enable)},
  {.type = CONF_TYPE_BOOL, .key = "debug_svc_enable", .offset = offsetof(struct mgos_config, bt.debug_svc_enable)},
  {.type = CONF_TYPE_OBJECT, .key = "file_logger", .num_desc = 5},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, file_logger.enable)},
  {.type = CONF_TYPE_STRING, .key = "dir", .offset = offsetof(struct mgos_config, file_logger.dir)},
  {.type = CONF_TYPE_STRING, .key = "prefix", .offset = offsetof(struct mgos_config, file_logger.prefix)},
  {.type = CONF_TYPE_INT, .key = "max_file_size", .offset = offsetof(struct mgos_config, file_logger.max_file_size)},
  {.type = CONF_TYPE_INT, .key = "max_num_files", .offset = offsetof(struct mgos_config, file_logger.max_num_files)},
  {.type = CONF_TYPE_OBJECT, .key = "wifi", .num_desc = 30},
  {.type = CONF_TYPE_OBJECT, .key = "sta", .num_desc = 13},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, wifi.sta.enable)},
  {.type = CONF_TYPE_STRING, .key = "ssid", .offset = offsetof(struct mgos_config, wifi.sta.ssid)},
  {.type = CONF_TYPE_STRING, .key = "pass", .offset = offsetof(struct mgos_config, wifi.sta.pass)},
  {.type = CONF_TYPE_STRING, .key = "user", .offset = offsetof(struct mgos_config, wifi.sta.user)},
  {.type = CONF_TYPE_STRING, .key = "anon_identity", .offset = offsetof(struct mgos_config, wifi.sta.anon_identity)},
  {.type = CONF_TYPE_STRING, .key = "cert", .offset = offsetof(struct mgos_config, wifi.sta.cert)},
  {.type = CONF_TYPE_STRING, .key = "key", .offset = offsetof(struct mgos_config, wifi.sta.key)},
  {.type = CONF_TYPE_STRING, .key = "ca_cert", .offset = offsetof(struct mgos_config, wifi.sta.ca_cert)},
  {.type = CONF_TYPE_STRING, .key = "ip", .offset = offsetof(struct mgos_config, wifi.sta.ip)},
  {.type = CONF_TYPE_STRING, .key = "netmask", .offset = offsetof(struct mgos_config, wifi.sta.netmask)},
  {.type = CONF_TYPE_STRING, .key = "gw", .offset = offsetof(struct mgos_config, wifi.sta.gw)},
  {.type = CONF_TYPE_STRING, .key = "nameserver", .offset = offsetof(struct mgos_config, wifi.sta.nameserver)},
  {.type = CONF_TYPE_STRING, .key = "dhcp_hostname", .offset = offsetof(struct mgos_config, wifi.sta.dhcp_hostname)},
  {.type = CONF_TYPE_OBJECT, .key = "ap", .num_desc = 15},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, wifi.ap.enable)},
  {.type = CONF_TYPE_STRING, .key = "ssid", .offset = offsetof(struct mgos_config, wifi.ap.ssid)},
  {.type = CONF_TYPE_STRING, .key = "pass", .offset = offsetof(struct mgos_config, wifi.ap.pass)},
  {.type = CONF_TYPE_BOOL, .key = "hidden", .offset = offsetof(struct mgos_config, wifi.ap.hidden)},
  {.type = CONF_TYPE_INT, .key = "channel", .offset = offsetof(struct mgos_config, wifi.ap.channel)},
  {.type = CONF_TYPE_INT, .key = "max_connections", .offset = offsetof(struct mgos_config, wifi.ap.max_connections)},
  {.type = CONF_TYPE_STRING, .key = "ip", .offset = offsetof(struct mgos_config, wifi.ap.ip)},
  {.type = CONF_TYPE_STRING, .key = "netmask", .offset = offsetof(struct mgos_config, wifi.ap.netmask)},
  {.type = CONF_TYPE_STRING, .key = "gw", .offset = offsetof(struct mgos_config, wifi.ap.gw)},
  {.type = CONF_TYPE_STRING, .key = "dhcp_start", .offset = offsetof(struct mgos_config, wifi.ap.dhcp_start)},
  {.type = CONF_TYPE_STRING, .key = "dhcp_end", .offset = offsetof(struct mgos_config, wifi.ap.dhcp_end)},
  {.type = CONF_TYPE_INT, .key = "trigger_on_gpio", .offset = offsetof(struct mgos_config, wifi.ap.trigger_on_gpio)},
  {.type = CONF_TYPE_INT, .key = "disable_after", .offset = offsetof(struct mgos_config, wifi.ap.disable_after)},
  {.type = CONF_TYPE_STRING, .key = "hostname", .offset = offsetof(struct mgos_config, wifi.ap.hostname)},
  {.type = CONF_TYPE_BOOL, .key = "keep_enabled", .offset = offsetof(struct mgos_config, wifi.ap.keep_enabled)},
  {.type = CONF_TYPE_OBJECT, .key = "http", .num_desc = 10},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, http.enable)},
  {.type = CONF_TYPE_STRING, .key = "listen_addr", .offset = offsetof(struct mgos_config, http.listen_addr)},
  {.type = CONF_TYPE_STRING, .key = "document_root", .offset = offsetof(struct mgos_config, http.document_root)},
  {.type = CONF_TYPE_STRING, .key = "ssl_cert", .offset = offsetof(struct mgos_config, http.ssl_cert)},
  {.type = CONF_TYPE_STRING, .key = "ssl_key", .offset = offsetof(struct mgos_config, http.ssl_key)},
  {.type = CONF_TYPE_STRING, .key = "ssl_ca_cert", .offset = offsetof(struct mgos_config, http.ssl_ca_cert)},
  {.type = CONF_TYPE_STRING, .key = "upload_acl", .offset = offsetof(struct mgos_config, http.upload_acl)},
  {.type = CONF_TYPE_STRING, .key = "hidden_files", .offset = offsetof(struct mgos_config, http.hidden_files)},
  {.type = CONF_TYPE_STRING, .key = "auth_domain", .offset = offsetof(struct mgos_config, http.auth_domain)},
  {.type = CONF_TYPE_STRING, .key = "auth_file", .offset = offsetof(struct mgos_config, http.auth_file)},
  {.type = CONF_TYPE_OBJECT, .key = "i2c", .num_desc = 6},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, i2c.enable)},
  {.type = CONF_TYPE_INT, .key = "freq", .offset = offsetof(struct mgos_config, i2c.freq)},
  {.type = CONF_TYPE_BOOL, .key = "debug", .offset = offsetof(struct mgos_config, i2c.debug)},
  {.type = CONF_TYPE_INT, .key = "unit_no", .offset = offsetof(struct mgos_config, i2c.unit_no)},
  {.type = CONF_TYPE_INT, .key = "sda_gpio", .offset = offsetof(struct mgos_config, i2c.sda_gpio)},
  {.type = CONF_TYPE_INT, .key = "scl_gpio", .offset = offsetof(struct mgos_config, i2c.scl_gpio)},
  {.type = CONF_TYPE_OBJECT, .key = "mjs", .num_desc = 1},
  {.type = CONF_TYPE_BOOL, .key = "generate_jsc", .offset = offsetof(struct mgos_config, mjs.generate_jsc)},
  {.type = CONF_TYPE_OBJECT, .key = "rpc", .num_desc = 23},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, rpc.enable)},
  {.type = CONF_TYPE_INT, .key = "max_frame_size", .offset = offsetof(struct mgos_config, rpc.max_frame_size)},
  {.type = CONF_TYPE_INT, .key = "max_queue_length", .offset = offsetof(struct mgos_config, rpc.max_queue_length)},
  {.type = CONF_TYPE_INT, .key = "default_out_channel_idle_close_timeout", .offset = offsetof(struct mgos_config, rpc.default_out_channel_idle_close_timeout)},
  {.type = CONF_TYPE_STRING, .key = "acl_file", .offset = offsetof(struct mgos_config, rpc.acl_file)},
  {.type = CONF_TYPE_STRING, .key = "auth_domain", .offset = offsetof(struct mgos_config, rpc.auth_domain)},
  {.type = CONF_TYPE_STRING, .key = "auth_file", .offset = offsetof(struct mgos_config, rpc.auth_file)},
  {.type = CONF_TYPE_OBJECT, .key = "ws", .num_desc = 7},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, rpc.ws.enable)},
  {.type = CONF_TYPE_STRING, .key = "server_address", .offset = offsetof(struct mgos_config, rpc.ws.server_address)},
  {.type = CONF_TYPE_INT, .key = "reconnect_interval_min", .offset = offsetof(struct mgos_config, rpc.ws.reconnect_interval_min)},
  {.type = CONF_TYPE_INT, .key = "reconnect_interval_max", .offset = offsetof(struct mgos_config, rpc.ws.reconnect_interval_max)},
  {.type = CONF_TYPE_STRING, .key = "ssl_server_name", .offset = offsetof(struct mgos_config, rpc.ws.ssl_server_name)},
  {.type = CONF_TYPE_STRING, .key = "ssl_ca_file", .offset = offsetof(struct mgos_config, rpc.ws.ssl_ca_file)},
  {.type = CONF_TYPE_STRING, .key = "ssl_client_cert_file", .offset = offsetof(struct mgos_config, rpc.ws.ssl_client_cert_file)},
  {.type = CONF_TYPE_OBJECT, .key = "mqtt", .num_desc = 2},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, rpc.mqtt.enable)},
  {.type = CONF_TYPE_STRING, .key = "topic", .offset = offsetof(struct mgos_config, rpc.mqtt.topic)},
  {.type = CONF_TYPE_OBJECT, .key = "uart", .num_desc = 4},
  {.type = CONF_TYPE_INT, .key = "uart_no", .offset = offsetof(struct mgos_config, rpc.uart.uart_no)},
  {.type = CONF_TYPE_INT, .key = "baud_rate", .offset = offsetof(struct mgos_config, rpc.uart.baud_rate)},
  {.type = CONF_TYPE_INT, .key = "fc_type", .offset = offsetof(struct mgos_config, rpc.uart.fc_type)},
  {.type = CONF_TYPE_BOOL, .key = "wait_for_start_frame", .offset = offsetof(struct mgos_config, rpc.uart.wait_for_start_frame)},
  {.type = CONF_TYPE_OBJECT, .key = "mqtt", .num_desc = 19},
  {.type = CONF_TYPE_BOOL, .key = "enable", .offset = offsetof(struct mgos_config, mqtt.enable)},
  {.type = CONF_TYPE_STRING, .key = "server", .offset = offsetof(struct mgos_config, mqtt.server)},
  {.type = CONF_TYPE_STRING, .key = "client_id", .offset = offsetof(struct mgos_config, mqtt.client_id)},
  {.type = CONF_TYPE_STRING, .key = "user", .offset = offsetof(struct mgos_config, mqtt.user)},
  {.type = CONF_TYPE_STRING, .key = "pass", .offset = offsetof(struct mgos_config, mqtt.pass)},
  {.type = CONF_TYPE_DOUBLE, .key = "reconnect_timeout_min", .offset = offsetof(struct mgos_config, mqtt.reconnect_timeout_min)},
  {.type = CONF_TYPE_DOUBLE, .key = "reconnect_timeout_max", .offset = offsetof(struct mgos_config, mqtt.reconnect_timeout_max)},
  {.type = CONF_TYPE_STRING, .key = "ssl_cert", .offset = offsetof(struct mgos_config, mqtt.ssl_cert)},
  {.type = CONF_TYPE_STRING, .key = "ssl_key", .offset = offsetof(struct mgos_config, mqtt.ssl_key)},
  {.type = CONF_TYPE_STRING, .key = "ssl_ca_cert", .offset = offsetof(struct mgos_config, mqtt.ssl_ca_cert)},
  {.type = CONF_TYPE_STRING, .key = "ssl_cipher_suites", .offset = offsetof(struct mgos_config, mqtt.ssl_cipher_suites)},
  {.type = CONF_TYPE_STRING, .key = "ssl_psk_identity", .offset = offsetof(struct mgos_config, mqtt.ssl_psk_identity)},
  {.type = CONF_TYPE_STRING, .key = "ssl_psk_key", .offset = offsetof(struct mgos_config, mqtt.ssl_psk_key)},
  {.type = CONF_TYPE_BOOL, .key = "clean_session", .offset = offsetof(struct mgos_config, mqtt.clean_session)},
  {.type = CONF_TYPE_INT, .key = "keep_alive", .offset = offsetof(struct mgos_config, mqtt.keep_alive)},
  {.type = CONF_TYPE_STRING, .key = "will_topic", .offset = offsetof(struct mgos_config, mqtt.will_topic)},
  {.type = CONF_TYPE_STRING, .key = "will_message", .offset = offsetof(struct mgos_config, mqtt.will_message)},
  {.type = CONF_TYPE_INT, .key = "max_qos", .offset = offsetof(struct mgos_config, mqtt.max_qos)},
  {.type = CONF_TYPE_INT, .key = "recv_mbuf_limit", .offset = offsetof(struct mgos_config, mqtt.recv_mbuf_limit)},
  {.type = CONF_TYPE_OBJECT, .key = "mcserver", .num_desc = 2},
  {.type = CONF_TYPE_STRING, .key = "id", .offset = offsetof(struct mgos_config, mcserver.id)},
  {.type = CONF_TYPE_STRING, .key = "topicName", .offset = offsetof(struct mgos_config, mcserver.topicName)},
  {.type = CONF_TYPE_OBJECT, .key = "lightModule", .num_desc = 12},
  {.type = CONF_TYPE_STRING, .key = "id", .offset = offsetof(struct mgos_config, lightModule.id)},
  {.type = CONF_TYPE_INT, .key = "lightSourcePin", .offset = offsetof(struct mgos_config, lightModule.lightSourcePin)},
  {.type = CONF_TYPE_INT, .key = "lightSensorPin", .offset = offsetof(struct mgos_config, lightModule.lightSensorPin)},
  {.type = CONF_TYPE_OBJECT, .key = "state", .num_desc = 8},
  {.type = CONF_TYPE_BOOL, .key = "lightTurnedOn", .offset = offsetof(struct mgos_config, lightModule.state.lightTurnedOn)},
  {.type = CONF_TYPE_STRING, .key = "mode", .offset = offsetof(struct mgos_config, lightModule.state.mode)},
  {.type = CONF_TYPE_BOOL, .key = "lightLvlCheckActive", .offset = offsetof(struct mgos_config, lightModule.state.lightLvlCheckActive)},
  {.type = CONF_TYPE_INT, .key = "lightLvlCheckInterval", .offset = offsetof(struct mgos_config, lightModule.state.lightLvlCheckInterval)},
  {.type = CONF_TYPE_INT, .key = "lightLvlCheckLastIntervalCallTimestamp", .offset = offsetof(struct mgos_config, lightModule.state.lightLvlCheckLastIntervalCallTimestamp)},
  {.type = CONF_TYPE_BOOL, .key = "lightIntervalsRestTimeTurnedOn", .offset = offsetof(struct mgos_config, lightModule.state.lightIntervalsRestTimeTurnedOn)},
  {.type = CONF_TYPE_INT, .key = "lightIntervalsCheckingInterval", .offset = offsetof(struct mgos_config, lightModule.state.lightIntervalsCheckingInterval)},
  {.type = CONF_TYPE_STRING, .key = "lightIntervalsArr", .offset = offsetof(struct mgos_config, lightModule.state.lightIntervalsArr)},
  {.type = CONF_TYPE_OBJECT, .key = "irrigationModule", .num_desc = 16},
  {.type = CONF_TYPE_STRING, .key = "id", .offset = offsetof(struct mgos_config, irrigationModule.id)},
  {.type = CONF_TYPE_INT, .key = "pumpPin", .offset = offsetof(struct mgos_config, irrigationModule.pumpPin)},
  {.type = CONF_TYPE_OBJECT, .key = "state", .num_desc = 13},
  {.type = CONF_TYPE_BOOL, .key = "irrigationTurnedOn", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTurnedOn)},
  {.type = CONF_TYPE_STRING, .key = "mode", .offset = offsetof(struct mgos_config, irrigationModule.state.mode)},
  {.type = CONF_TYPE_BOOL, .key = "humidityCheckActive", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckActive)},
  {.type = CONF_TYPE_INT, .key = "humidityCheckInterval", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckInterval)},
  {.type = CONF_TYPE_INT, .key = "humidityCheckLastIntervalCallTimestamp", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckLastIntervalCallTimestamp)},
  {.type = CONF_TYPE_INT, .key = "humidityCheckMinLvl", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckMinLvl)},
  {.type = CONF_TYPE_INT, .key = "humidityCheckAverageLvl", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckAverageLvl)},
  {.type = CONF_TYPE_INT, .key = "humidityCheckMaxLvl", .offset = offsetof(struct mgos_config, irrigationModule.state.humidityCheckMaxLvl)},
  {.type = CONF_TYPE_BOOL, .key = "irrigationTimerInProgress", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTimerInProgress)},
  {.type = CONF_TYPE_INT, .key = "irrigationTimerEveryXSeconds", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTimerEveryXSeconds)},
  {.type = CONF_TYPE_INT, .key = "irrigationTimerIrrigateYSeconds", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTimerIrrigateYSeconds)},
  {.type = CONF_TYPE_INT, .key = "irrigationTimerLastCallStartTimestamp", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTimerLastCallStartTimestamp)},
  {.type = CONF_TYPE_INT, .key = "irrigationTimerLastCallEndTimestamp", .offset = offsetof(struct mgos_config, irrigationModule.state.irrigationTimerLastCallEndTimestamp)},
};

const struct mgos_conf_entry *mgos_config_schema() {
  return mgos_config_schema_;
}

/* Global instance */
struct mgos_config mgos_sys_config;

/* Getters {{{ */
const struct mgos_config_update *mgos_config_get_update(struct mgos_config *cfg) {
  return &cfg->update;
}
int         mgos_config_get_update_timeout(struct mgos_config *cfg) {
  return cfg->update.timeout;
}
int         mgos_config_get_update_commit_timeout(struct mgos_config *cfg) {
  return cfg->update.commit_timeout;
}
const char *mgos_config_get_update_url(struct mgos_config *cfg) {
  return cfg->update.url;
}
int         mgos_config_get_update_interval(struct mgos_config *cfg) {
  return cfg->update.interval;
}
const char *mgos_config_get_update_ssl_ca_file(struct mgos_config *cfg) {
  return cfg->update.ssl_ca_file;
}
const char *mgos_config_get_update_ssl_client_cert_file(struct mgos_config *cfg) {
  return cfg->update.ssl_client_cert_file;
}
const char *mgos_config_get_update_ssl_server_name(struct mgos_config *cfg) {
  return cfg->update.ssl_server_name;
}
int         mgos_config_get_update_enable_post(struct mgos_config *cfg) {
  return cfg->update.enable_post;
}
const struct mgos_config_device *mgos_config_get_device(struct mgos_config *cfg) {
  return &cfg->device;
}
const char *mgos_config_get_device_id(struct mgos_config *cfg) {
  return cfg->device.id;
}
const char *mgos_config_get_device_password(struct mgos_config *cfg) {
  return cfg->device.password;
}
const char *mgos_config_get_device_type(struct mgos_config *cfg) {
  return cfg->device.type;
}
const char *mgos_config_get_device_rpcPrefix(struct mgos_config *cfg) {
  return cfg->device.rpcPrefix;
}
const struct mgos_config_debug *mgos_config_get_debug(struct mgos_config *cfg) {
  return &cfg->debug;
}
const char *mgos_config_get_debug_udp_log_addr(struct mgos_config *cfg) {
  return cfg->debug.udp_log_addr;
}
int         mgos_config_get_debug_level(struct mgos_config *cfg) {
  return cfg->debug.level;
}
const char *mgos_config_get_debug_filter(struct mgos_config *cfg) {
  return cfg->debug.filter;
}
int         mgos_config_get_debug_stdout_uart(struct mgos_config *cfg) {
  return cfg->debug.stdout_uart;
}
int         mgos_config_get_debug_stderr_uart(struct mgos_config *cfg) {
  return cfg->debug.stderr_uart;
}
int         mgos_config_get_debug_factory_reset_gpio(struct mgos_config *cfg) {
  return cfg->debug.factory_reset_gpio;
}
const char *mgos_config_get_debug_mg_mgr_hexdump_file(struct mgos_config *cfg) {
  return cfg->debug.mg_mgr_hexdump_file;
}
int         mgos_config_get_debug_mbedtls_level(struct mgos_config *cfg) {
  return cfg->debug.mbedtls_level;
}
const char *mgos_config_get_debug_stdout_topic(struct mgos_config *cfg) {
  return cfg->debug.stdout_topic;
}
const char *mgos_config_get_debug_stderr_topic(struct mgos_config *cfg) {
  return cfg->debug.stderr_topic;
}
const struct mgos_config_sys *mgos_config_get_sys(struct mgos_config *cfg) {
  return &cfg->sys;
}
const struct mgos_config_sys_mount *mgos_config_get_sys_mount(struct mgos_config *cfg) {
  return &cfg->sys.mount;
}
const char *mgos_config_get_sys_mount_path(struct mgos_config *cfg) {
  return cfg->sys.mount.path;
}
const char *mgos_config_get_sys_mount_dev_type(struct mgos_config *cfg) {
  return cfg->sys.mount.dev_type;
}
const char *mgos_config_get_sys_mount_dev_opts(struct mgos_config *cfg) {
  return cfg->sys.mount.dev_opts;
}
const char *mgos_config_get_sys_mount_fs_type(struct mgos_config *cfg) {
  return cfg->sys.mount.fs_type;
}
const char *mgos_config_get_sys_mount_fs_opts(struct mgos_config *cfg) {
  return cfg->sys.mount.fs_opts;
}
const char *mgos_config_get_sys_tz_spec(struct mgos_config *cfg) {
  return cfg->sys.tz_spec;
}
int         mgos_config_get_sys_wdt_timeout(struct mgos_config *cfg) {
  return cfg->sys.wdt_timeout;
}
const char *mgos_config_get_sys_pref_ota_lib(struct mgos_config *cfg) {
  return cfg->sys.pref_ota_lib;
}
int         mgos_config_get_sys_esp32_adc_vref(struct mgos_config *cfg) {
  return cfg->sys.esp32_adc_vref;
}
const char *mgos_config_get_conf_acl(struct mgos_config *cfg) {
  return cfg->conf_acl;
}
const struct mgos_config_bt *mgos_config_get_bt(struct mgos_config *cfg) {
  return &cfg->bt;
}
int         mgos_config_get_bt_enable(struct mgos_config *cfg) {
  return cfg->bt.enable;
}
const char *mgos_config_get_bt_dev_name(struct mgos_config *cfg) {
  return cfg->bt.dev_name;
}
int         mgos_config_get_bt_adv_enable(struct mgos_config *cfg) {
  return cfg->bt.adv_enable;
}
const char *mgos_config_get_bt_scan_rsp_data_hex(struct mgos_config *cfg) {
  return cfg->bt.scan_rsp_data_hex;
}
int         mgos_config_get_bt_keep_enabled(struct mgos_config *cfg) {
  return cfg->bt.keep_enabled;
}
int         mgos_config_get_bt_allow_pairing(struct mgos_config *cfg) {
  return cfg->bt.allow_pairing;
}
int         mgos_config_get_bt_max_paired_devices(struct mgos_config *cfg) {
  return cfg->bt.max_paired_devices;
}
int         mgos_config_get_bt_random_address(struct mgos_config *cfg) {
  return cfg->bt.random_address;
}
const struct mgos_config_bt_gatts *mgos_config_get_bt_gatts(struct mgos_config *cfg) {
  return &cfg->bt.gatts;
}
int         mgos_config_get_bt_gatts_min_sec_level(struct mgos_config *cfg) {
  return cfg->bt.gatts.min_sec_level;
}
int         mgos_config_get_bt_gatts_require_pairing(struct mgos_config *cfg) {
  return cfg->bt.gatts.require_pairing;
}
int         mgos_config_get_bt_config_svc_enable(struct mgos_config *cfg) {
  return cfg->bt.config_svc_enable;
}
int         mgos_config_get_bt_debug_svc_enable(struct mgos_config *cfg) {
  return cfg->bt.debug_svc_enable;
}
const struct mgos_config_file_logger *mgos_config_get_file_logger(struct mgos_config *cfg) {
  return &cfg->file_logger;
}
int         mgos_config_get_file_logger_enable(struct mgos_config *cfg) {
  return cfg->file_logger.enable;
}
const char *mgos_config_get_file_logger_dir(struct mgos_config *cfg) {
  return cfg->file_logger.dir;
}
const char *mgos_config_get_file_logger_prefix(struct mgos_config *cfg) {
  return cfg->file_logger.prefix;
}
int         mgos_config_get_file_logger_max_file_size(struct mgos_config *cfg) {
  return cfg->file_logger.max_file_size;
}
int         mgos_config_get_file_logger_max_num_files(struct mgos_config *cfg) {
  return cfg->file_logger.max_num_files;
}
const struct mgos_config_wifi *mgos_config_get_wifi(struct mgos_config *cfg) {
  return &cfg->wifi;
}
const struct mgos_config_wifi_sta *mgos_config_get_wifi_sta(struct mgos_config *cfg) {
  return &cfg->wifi.sta;
}
int         mgos_config_get_wifi_sta_enable(struct mgos_config *cfg) {
  return cfg->wifi.sta.enable;
}
const char *mgos_config_get_wifi_sta_ssid(struct mgos_config *cfg) {
  return cfg->wifi.sta.ssid;
}
const char *mgos_config_get_wifi_sta_pass(struct mgos_config *cfg) {
  return cfg->wifi.sta.pass;
}
const char *mgos_config_get_wifi_sta_user(struct mgos_config *cfg) {
  return cfg->wifi.sta.user;
}
const char *mgos_config_get_wifi_sta_anon_identity(struct mgos_config *cfg) {
  return cfg->wifi.sta.anon_identity;
}
const char *mgos_config_get_wifi_sta_cert(struct mgos_config *cfg) {
  return cfg->wifi.sta.cert;
}
const char *mgos_config_get_wifi_sta_key(struct mgos_config *cfg) {
  return cfg->wifi.sta.key;
}
const char *mgos_config_get_wifi_sta_ca_cert(struct mgos_config *cfg) {
  return cfg->wifi.sta.ca_cert;
}
const char *mgos_config_get_wifi_sta_ip(struct mgos_config *cfg) {
  return cfg->wifi.sta.ip;
}
const char *mgos_config_get_wifi_sta_netmask(struct mgos_config *cfg) {
  return cfg->wifi.sta.netmask;
}
const char *mgos_config_get_wifi_sta_gw(struct mgos_config *cfg) {
  return cfg->wifi.sta.gw;
}
const char *mgos_config_get_wifi_sta_nameserver(struct mgos_config *cfg) {
  return cfg->wifi.sta.nameserver;
}
const char *mgos_config_get_wifi_sta_dhcp_hostname(struct mgos_config *cfg) {
  return cfg->wifi.sta.dhcp_hostname;
}
const struct mgos_config_wifi_ap *mgos_config_get_wifi_ap(struct mgos_config *cfg) {
  return &cfg->wifi.ap;
}
int         mgos_config_get_wifi_ap_enable(struct mgos_config *cfg) {
  return cfg->wifi.ap.enable;
}
const char *mgos_config_get_wifi_ap_ssid(struct mgos_config *cfg) {
  return cfg->wifi.ap.ssid;
}
const char *mgos_config_get_wifi_ap_pass(struct mgos_config *cfg) {
  return cfg->wifi.ap.pass;
}
int         mgos_config_get_wifi_ap_hidden(struct mgos_config *cfg) {
  return cfg->wifi.ap.hidden;
}
int         mgos_config_get_wifi_ap_channel(struct mgos_config *cfg) {
  return cfg->wifi.ap.channel;
}
int         mgos_config_get_wifi_ap_max_connections(struct mgos_config *cfg) {
  return cfg->wifi.ap.max_connections;
}
const char *mgos_config_get_wifi_ap_ip(struct mgos_config *cfg) {
  return cfg->wifi.ap.ip;
}
const char *mgos_config_get_wifi_ap_netmask(struct mgos_config *cfg) {
  return cfg->wifi.ap.netmask;
}
const char *mgos_config_get_wifi_ap_gw(struct mgos_config *cfg) {
  return cfg->wifi.ap.gw;
}
const char *mgos_config_get_wifi_ap_dhcp_start(struct mgos_config *cfg) {
  return cfg->wifi.ap.dhcp_start;
}
const char *mgos_config_get_wifi_ap_dhcp_end(struct mgos_config *cfg) {
  return cfg->wifi.ap.dhcp_end;
}
int         mgos_config_get_wifi_ap_trigger_on_gpio(struct mgos_config *cfg) {
  return cfg->wifi.ap.trigger_on_gpio;
}
int         mgos_config_get_wifi_ap_disable_after(struct mgos_config *cfg) {
  return cfg->wifi.ap.disable_after;
}
const char *mgos_config_get_wifi_ap_hostname(struct mgos_config *cfg) {
  return cfg->wifi.ap.hostname;
}
int         mgos_config_get_wifi_ap_keep_enabled(struct mgos_config *cfg) {
  return cfg->wifi.ap.keep_enabled;
}
const struct mgos_config_http *mgos_config_get_http(struct mgos_config *cfg) {
  return &cfg->http;
}
int         mgos_config_get_http_enable(struct mgos_config *cfg) {
  return cfg->http.enable;
}
const char *mgos_config_get_http_listen_addr(struct mgos_config *cfg) {
  return cfg->http.listen_addr;
}
const char *mgos_config_get_http_document_root(struct mgos_config *cfg) {
  return cfg->http.document_root;
}
const char *mgos_config_get_http_ssl_cert(struct mgos_config *cfg) {
  return cfg->http.ssl_cert;
}
const char *mgos_config_get_http_ssl_key(struct mgos_config *cfg) {
  return cfg->http.ssl_key;
}
const char *mgos_config_get_http_ssl_ca_cert(struct mgos_config *cfg) {
  return cfg->http.ssl_ca_cert;
}
const char *mgos_config_get_http_upload_acl(struct mgos_config *cfg) {
  return cfg->http.upload_acl;
}
const char *mgos_config_get_http_hidden_files(struct mgos_config *cfg) {
  return cfg->http.hidden_files;
}
const char *mgos_config_get_http_auth_domain(struct mgos_config *cfg) {
  return cfg->http.auth_domain;
}
const char *mgos_config_get_http_auth_file(struct mgos_config *cfg) {
  return cfg->http.auth_file;
}
const struct mgos_config_i2c *mgos_config_get_i2c(struct mgos_config *cfg) {
  return &cfg->i2c;
}
int         mgos_config_get_i2c_enable(struct mgos_config *cfg) {
  return cfg->i2c.enable;
}
int         mgos_config_get_i2c_freq(struct mgos_config *cfg) {
  return cfg->i2c.freq;
}
int         mgos_config_get_i2c_debug(struct mgos_config *cfg) {
  return cfg->i2c.debug;
}
int         mgos_config_get_i2c_unit_no(struct mgos_config *cfg) {
  return cfg->i2c.unit_no;
}
int         mgos_config_get_i2c_sda_gpio(struct mgos_config *cfg) {
  return cfg->i2c.sda_gpio;
}
int         mgos_config_get_i2c_scl_gpio(struct mgos_config *cfg) {
  return cfg->i2c.scl_gpio;
}
const struct mgos_config_mjs *mgos_config_get_mjs(struct mgos_config *cfg) {
  return &cfg->mjs;
}
int         mgos_config_get_mjs_generate_jsc(struct mgos_config *cfg) {
  return cfg->mjs.generate_jsc;
}
const struct mgos_config_rpc *mgos_config_get_rpc(struct mgos_config *cfg) {
  return &cfg->rpc;
}
int         mgos_config_get_rpc_enable(struct mgos_config *cfg) {
  return cfg->rpc.enable;
}
int         mgos_config_get_rpc_max_frame_size(struct mgos_config *cfg) {
  return cfg->rpc.max_frame_size;
}
int         mgos_config_get_rpc_max_queue_length(struct mgos_config *cfg) {
  return cfg->rpc.max_queue_length;
}
int         mgos_config_get_rpc_default_out_channel_idle_close_timeout(struct mgos_config *cfg) {
  return cfg->rpc.default_out_channel_idle_close_timeout;
}
const char *mgos_config_get_rpc_acl_file(struct mgos_config *cfg) {
  return cfg->rpc.acl_file;
}
const char *mgos_config_get_rpc_auth_domain(struct mgos_config *cfg) {
  return cfg->rpc.auth_domain;
}
const char *mgos_config_get_rpc_auth_file(struct mgos_config *cfg) {
  return cfg->rpc.auth_file;
}
const struct mgos_config_rpc_ws *mgos_config_get_rpc_ws(struct mgos_config *cfg) {
  return &cfg->rpc.ws;
}
int         mgos_config_get_rpc_ws_enable(struct mgos_config *cfg) {
  return cfg->rpc.ws.enable;
}
const char *mgos_config_get_rpc_ws_server_address(struct mgos_config *cfg) {
  return cfg->rpc.ws.server_address;
}
int         mgos_config_get_rpc_ws_reconnect_interval_min(struct mgos_config *cfg) {
  return cfg->rpc.ws.reconnect_interval_min;
}
int         mgos_config_get_rpc_ws_reconnect_interval_max(struct mgos_config *cfg) {
  return cfg->rpc.ws.reconnect_interval_max;
}
const char *mgos_config_get_rpc_ws_ssl_server_name(struct mgos_config *cfg) {
  return cfg->rpc.ws.ssl_server_name;
}
const char *mgos_config_get_rpc_ws_ssl_ca_file(struct mgos_config *cfg) {
  return cfg->rpc.ws.ssl_ca_file;
}
const char *mgos_config_get_rpc_ws_ssl_client_cert_file(struct mgos_config *cfg) {
  return cfg->rpc.ws.ssl_client_cert_file;
}
const struct mgos_config_rpc_mqtt *mgos_config_get_rpc_mqtt(struct mgos_config *cfg) {
  return &cfg->rpc.mqtt;
}
int         mgos_config_get_rpc_mqtt_enable(struct mgos_config *cfg) {
  return cfg->rpc.mqtt.enable;
}
const char *mgos_config_get_rpc_mqtt_topic(struct mgos_config *cfg) {
  return cfg->rpc.mqtt.topic;
}
const struct mgos_config_rpc_uart *mgos_config_get_rpc_uart(struct mgos_config *cfg) {
  return &cfg->rpc.uart;
}
int         mgos_config_get_rpc_uart_uart_no(struct mgos_config *cfg) {
  return cfg->rpc.uart.uart_no;
}
int         mgos_config_get_rpc_uart_baud_rate(struct mgos_config *cfg) {
  return cfg->rpc.uart.baud_rate;
}
int         mgos_config_get_rpc_uart_fc_type(struct mgos_config *cfg) {
  return cfg->rpc.uart.fc_type;
}
int         mgos_config_get_rpc_uart_wait_for_start_frame(struct mgos_config *cfg) {
  return cfg->rpc.uart.wait_for_start_frame;
}
const struct mgos_config_mqtt *mgos_config_get_mqtt(struct mgos_config *cfg) {
  return &cfg->mqtt;
}
int         mgos_config_get_mqtt_enable(struct mgos_config *cfg) {
  return cfg->mqtt.enable;
}
const char *mgos_config_get_mqtt_server(struct mgos_config *cfg) {
  return cfg->mqtt.server;
}
const char *mgos_config_get_mqtt_client_id(struct mgos_config *cfg) {
  return cfg->mqtt.client_id;
}
const char *mgos_config_get_mqtt_user(struct mgos_config *cfg) {
  return cfg->mqtt.user;
}
const char *mgos_config_get_mqtt_pass(struct mgos_config *cfg) {
  return cfg->mqtt.pass;
}
double      mgos_config_get_mqtt_reconnect_timeout_min(struct mgos_config *cfg) {
  return cfg->mqtt.reconnect_timeout_min;
}
double      mgos_config_get_mqtt_reconnect_timeout_max(struct mgos_config *cfg) {
  return cfg->mqtt.reconnect_timeout_max;
}
const char *mgos_config_get_mqtt_ssl_cert(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_cert;
}
const char *mgos_config_get_mqtt_ssl_key(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_key;
}
const char *mgos_config_get_mqtt_ssl_ca_cert(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_ca_cert;
}
const char *mgos_config_get_mqtt_ssl_cipher_suites(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_cipher_suites;
}
const char *mgos_config_get_mqtt_ssl_psk_identity(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_psk_identity;
}
const char *mgos_config_get_mqtt_ssl_psk_key(struct mgos_config *cfg) {
  return cfg->mqtt.ssl_psk_key;
}
int         mgos_config_get_mqtt_clean_session(struct mgos_config *cfg) {
  return cfg->mqtt.clean_session;
}
int         mgos_config_get_mqtt_keep_alive(struct mgos_config *cfg) {
  return cfg->mqtt.keep_alive;
}
const char *mgos_config_get_mqtt_will_topic(struct mgos_config *cfg) {
  return cfg->mqtt.will_topic;
}
const char *mgos_config_get_mqtt_will_message(struct mgos_config *cfg) {
  return cfg->mqtt.will_message;
}
int         mgos_config_get_mqtt_max_qos(struct mgos_config *cfg) {
  return cfg->mqtt.max_qos;
}
int         mgos_config_get_mqtt_recv_mbuf_limit(struct mgos_config *cfg) {
  return cfg->mqtt.recv_mbuf_limit;
}
const struct mgos_config_mcserver *mgos_config_get_mcserver(struct mgos_config *cfg) {
  return &cfg->mcserver;
}
const char *mgos_config_get_mcserver_id(struct mgos_config *cfg) {
  return cfg->mcserver.id;
}
const char *mgos_config_get_mcserver_topicName(struct mgos_config *cfg) {
  return cfg->mcserver.topicName;
}
const struct mgos_config_lightModule *mgos_config_get_lightModule(struct mgos_config *cfg) {
  return &cfg->lightModule;
}
const char *mgos_config_get_lightModule_id(struct mgos_config *cfg) {
  return cfg->lightModule.id;
}
int         mgos_config_get_lightModule_lightSourcePin(struct mgos_config *cfg) {
  return cfg->lightModule.lightSourcePin;
}
int         mgos_config_get_lightModule_lightSensorPin(struct mgos_config *cfg) {
  return cfg->lightModule.lightSensorPin;
}
const struct mgos_config_lightModule_state *mgos_config_get_lightModule_state(struct mgos_config *cfg) {
  return &cfg->lightModule.state;
}
int         mgos_config_get_lightModule_state_lightTurnedOn(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightTurnedOn;
}
const char *mgos_config_get_lightModule_state_mode(struct mgos_config *cfg) {
  return cfg->lightModule.state.mode;
}
int         mgos_config_get_lightModule_state_lightLvlCheckActive(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightLvlCheckActive;
}
int         mgos_config_get_lightModule_state_lightLvlCheckInterval(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightLvlCheckInterval;
}
int         mgos_config_get_lightModule_state_lightLvlCheckLastIntervalCallTimestamp(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightLvlCheckLastIntervalCallTimestamp;
}
int         mgos_config_get_lightModule_state_lightIntervalsRestTimeTurnedOn(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightIntervalsRestTimeTurnedOn;
}
int         mgos_config_get_lightModule_state_lightIntervalsCheckingInterval(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightIntervalsCheckingInterval;
}
const char *mgos_config_get_lightModule_state_lightIntervalsArr(struct mgos_config *cfg) {
  return cfg->lightModule.state.lightIntervalsArr;
}
const struct mgos_config_irrigationModule *mgos_config_get_irrigationModule(struct mgos_config *cfg) {
  return &cfg->irrigationModule;
}
const char *mgos_config_get_irrigationModule_id(struct mgos_config *cfg) {
  return cfg->irrigationModule.id;
}
int         mgos_config_get_irrigationModule_pumpPin(struct mgos_config *cfg) {
  return cfg->irrigationModule.pumpPin;
}
const struct mgos_config_irrigationModule_state *mgos_config_get_irrigationModule_state(struct mgos_config *cfg) {
  return &cfg->irrigationModule.state;
}
int         mgos_config_get_irrigationModule_state_irrigationTurnedOn(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTurnedOn;
}
const char *mgos_config_get_irrigationModule_state_mode(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.mode;
}
int         mgos_config_get_irrigationModule_state_humidityCheckActive(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckActive;
}
int         mgos_config_get_irrigationModule_state_humidityCheckInterval(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckInterval;
}
int         mgos_config_get_irrigationModule_state_humidityCheckLastIntervalCallTimestamp(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckLastIntervalCallTimestamp;
}
int         mgos_config_get_irrigationModule_state_humidityCheckMinLvl(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckMinLvl;
}
int         mgos_config_get_irrigationModule_state_humidityCheckAverageLvl(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckAverageLvl;
}
int         mgos_config_get_irrigationModule_state_humidityCheckMaxLvl(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.humidityCheckMaxLvl;
}
int         mgos_config_get_irrigationModule_state_irrigationTimerInProgress(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTimerInProgress;
}
int         mgos_config_get_irrigationModule_state_irrigationTimerEveryXSeconds(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTimerEveryXSeconds;
}
int         mgos_config_get_irrigationModule_state_irrigationTimerIrrigateYSeconds(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTimerIrrigateYSeconds;
}
int         mgos_config_get_irrigationModule_state_irrigationTimerLastCallStartTimestamp(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTimerLastCallStartTimestamp;
}
int         mgos_config_get_irrigationModule_state_irrigationTimerLastCallEndTimestamp(struct mgos_config *cfg) {
  return cfg->irrigationModule.state.irrigationTimerLastCallEndTimestamp;
}
/* }}} */

/* Setters {{{ */
void mgos_config_set_update_timeout(struct mgos_config *cfg, int         val) {
  cfg->update.timeout = val;
}
void mgos_config_set_update_commit_timeout(struct mgos_config *cfg, int         val) {
  cfg->update.commit_timeout = val;
}
void mgos_config_set_update_url(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->update.url, val);
}
void mgos_config_set_update_interval(struct mgos_config *cfg, int         val) {
  cfg->update.interval = val;
}
void mgos_config_set_update_ssl_ca_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->update.ssl_ca_file, val);
}
void mgos_config_set_update_ssl_client_cert_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->update.ssl_client_cert_file, val);
}
void mgos_config_set_update_ssl_server_name(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->update.ssl_server_name, val);
}
void mgos_config_set_update_enable_post(struct mgos_config *cfg, int         val) {
  cfg->update.enable_post = val;
}
void mgos_config_set_device_id(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->device.id, val);
}
void mgos_config_set_device_password(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->device.password, val);
}
void mgos_config_set_device_type(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->device.type, val);
}
void mgos_config_set_device_rpcPrefix(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->device.rpcPrefix, val);
}
void mgos_config_set_debug_udp_log_addr(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->debug.udp_log_addr, val);
}
void mgos_config_set_debug_level(struct mgos_config *cfg, int         val) {
  cfg->debug.level = val;
}
void mgos_config_set_debug_filter(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->debug.filter, val);
}
void mgos_config_set_debug_stdout_uart(struct mgos_config *cfg, int         val) {
  cfg->debug.stdout_uart = val;
}
void mgos_config_set_debug_stderr_uart(struct mgos_config *cfg, int         val) {
  cfg->debug.stderr_uart = val;
}
void mgos_config_set_debug_factory_reset_gpio(struct mgos_config *cfg, int         val) {
  cfg->debug.factory_reset_gpio = val;
}
void mgos_config_set_debug_mg_mgr_hexdump_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->debug.mg_mgr_hexdump_file, val);
}
void mgos_config_set_debug_mbedtls_level(struct mgos_config *cfg, int         val) {
  cfg->debug.mbedtls_level = val;
}
void mgos_config_set_debug_stdout_topic(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->debug.stdout_topic, val);
}
void mgos_config_set_debug_stderr_topic(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->debug.stderr_topic, val);
}
void mgos_config_set_sys_mount_path(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.mount.path, val);
}
void mgos_config_set_sys_mount_dev_type(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.mount.dev_type, val);
}
void mgos_config_set_sys_mount_dev_opts(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.mount.dev_opts, val);
}
void mgos_config_set_sys_mount_fs_type(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.mount.fs_type, val);
}
void mgos_config_set_sys_mount_fs_opts(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.mount.fs_opts, val);
}
void mgos_config_set_sys_tz_spec(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.tz_spec, val);
}
void mgos_config_set_sys_wdt_timeout(struct mgos_config *cfg, int         val) {
  cfg->sys.wdt_timeout = val;
}
void mgos_config_set_sys_pref_ota_lib(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->sys.pref_ota_lib, val);
}
void mgos_config_set_sys_esp32_adc_vref(struct mgos_config *cfg, int         val) {
  cfg->sys.esp32_adc_vref = val;
}
void mgos_config_set_conf_acl(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->conf_acl, val);
}
void mgos_config_set_bt_enable(struct mgos_config *cfg, int         val) {
  cfg->bt.enable = val;
}
void mgos_config_set_bt_dev_name(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->bt.dev_name, val);
}
void mgos_config_set_bt_adv_enable(struct mgos_config *cfg, int         val) {
  cfg->bt.adv_enable = val;
}
void mgos_config_set_bt_scan_rsp_data_hex(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->bt.scan_rsp_data_hex, val);
}
void mgos_config_set_bt_keep_enabled(struct mgos_config *cfg, int         val) {
  cfg->bt.keep_enabled = val;
}
void mgos_config_set_bt_allow_pairing(struct mgos_config *cfg, int         val) {
  cfg->bt.allow_pairing = val;
}
void mgos_config_set_bt_max_paired_devices(struct mgos_config *cfg, int         val) {
  cfg->bt.max_paired_devices = val;
}
void mgos_config_set_bt_random_address(struct mgos_config *cfg, int         val) {
  cfg->bt.random_address = val;
}
void mgos_config_set_bt_gatts_min_sec_level(struct mgos_config *cfg, int         val) {
  cfg->bt.gatts.min_sec_level = val;
}
void mgos_config_set_bt_gatts_require_pairing(struct mgos_config *cfg, int         val) {
  cfg->bt.gatts.require_pairing = val;
}
void mgos_config_set_bt_config_svc_enable(struct mgos_config *cfg, int         val) {
  cfg->bt.config_svc_enable = val;
}
void mgos_config_set_bt_debug_svc_enable(struct mgos_config *cfg, int         val) {
  cfg->bt.debug_svc_enable = val;
}
void mgos_config_set_file_logger_enable(struct mgos_config *cfg, int         val) {
  cfg->file_logger.enable = val;
}
void mgos_config_set_file_logger_dir(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->file_logger.dir, val);
}
void mgos_config_set_file_logger_prefix(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->file_logger.prefix, val);
}
void mgos_config_set_file_logger_max_file_size(struct mgos_config *cfg, int         val) {
  cfg->file_logger.max_file_size = val;
}
void mgos_config_set_file_logger_max_num_files(struct mgos_config *cfg, int         val) {
  cfg->file_logger.max_num_files = val;
}
void mgos_config_set_wifi_sta_enable(struct mgos_config *cfg, int         val) {
  cfg->wifi.sta.enable = val;
}
void mgos_config_set_wifi_sta_ssid(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.ssid, val);
}
void mgos_config_set_wifi_sta_pass(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.pass, val);
}
void mgos_config_set_wifi_sta_user(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.user, val);
}
void mgos_config_set_wifi_sta_anon_identity(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.anon_identity, val);
}
void mgos_config_set_wifi_sta_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.cert, val);
}
void mgos_config_set_wifi_sta_key(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.key, val);
}
void mgos_config_set_wifi_sta_ca_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.ca_cert, val);
}
void mgos_config_set_wifi_sta_ip(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.ip, val);
}
void mgos_config_set_wifi_sta_netmask(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.netmask, val);
}
void mgos_config_set_wifi_sta_gw(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.gw, val);
}
void mgos_config_set_wifi_sta_nameserver(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.nameserver, val);
}
void mgos_config_set_wifi_sta_dhcp_hostname(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.sta.dhcp_hostname, val);
}
void mgos_config_set_wifi_ap_enable(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.enable = val;
}
void mgos_config_set_wifi_ap_ssid(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.ssid, val);
}
void mgos_config_set_wifi_ap_pass(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.pass, val);
}
void mgos_config_set_wifi_ap_hidden(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.hidden = val;
}
void mgos_config_set_wifi_ap_channel(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.channel = val;
}
void mgos_config_set_wifi_ap_max_connections(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.max_connections = val;
}
void mgos_config_set_wifi_ap_ip(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.ip, val);
}
void mgos_config_set_wifi_ap_netmask(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.netmask, val);
}
void mgos_config_set_wifi_ap_gw(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.gw, val);
}
void mgos_config_set_wifi_ap_dhcp_start(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.dhcp_start, val);
}
void mgos_config_set_wifi_ap_dhcp_end(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.dhcp_end, val);
}
void mgos_config_set_wifi_ap_trigger_on_gpio(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.trigger_on_gpio = val;
}
void mgos_config_set_wifi_ap_disable_after(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.disable_after = val;
}
void mgos_config_set_wifi_ap_hostname(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->wifi.ap.hostname, val);
}
void mgos_config_set_wifi_ap_keep_enabled(struct mgos_config *cfg, int         val) {
  cfg->wifi.ap.keep_enabled = val;
}
void mgos_config_set_http_enable(struct mgos_config *cfg, int         val) {
  cfg->http.enable = val;
}
void mgos_config_set_http_listen_addr(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.listen_addr, val);
}
void mgos_config_set_http_document_root(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.document_root, val);
}
void mgos_config_set_http_ssl_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.ssl_cert, val);
}
void mgos_config_set_http_ssl_key(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.ssl_key, val);
}
void mgos_config_set_http_ssl_ca_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.ssl_ca_cert, val);
}
void mgos_config_set_http_upload_acl(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.upload_acl, val);
}
void mgos_config_set_http_hidden_files(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.hidden_files, val);
}
void mgos_config_set_http_auth_domain(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.auth_domain, val);
}
void mgos_config_set_http_auth_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->http.auth_file, val);
}
void mgos_config_set_i2c_enable(struct mgos_config *cfg, int         val) {
  cfg->i2c.enable = val;
}
void mgos_config_set_i2c_freq(struct mgos_config *cfg, int         val) {
  cfg->i2c.freq = val;
}
void mgos_config_set_i2c_debug(struct mgos_config *cfg, int         val) {
  cfg->i2c.debug = val;
}
void mgos_config_set_i2c_unit_no(struct mgos_config *cfg, int         val) {
  cfg->i2c.unit_no = val;
}
void mgos_config_set_i2c_sda_gpio(struct mgos_config *cfg, int         val) {
  cfg->i2c.sda_gpio = val;
}
void mgos_config_set_i2c_scl_gpio(struct mgos_config *cfg, int         val) {
  cfg->i2c.scl_gpio = val;
}
void mgos_config_set_mjs_generate_jsc(struct mgos_config *cfg, int         val) {
  cfg->mjs.generate_jsc = val;
}
void mgos_config_set_rpc_enable(struct mgos_config *cfg, int         val) {
  cfg->rpc.enable = val;
}
void mgos_config_set_rpc_max_frame_size(struct mgos_config *cfg, int         val) {
  cfg->rpc.max_frame_size = val;
}
void mgos_config_set_rpc_max_queue_length(struct mgos_config *cfg, int         val) {
  cfg->rpc.max_queue_length = val;
}
void mgos_config_set_rpc_default_out_channel_idle_close_timeout(struct mgos_config *cfg, int         val) {
  cfg->rpc.default_out_channel_idle_close_timeout = val;
}
void mgos_config_set_rpc_acl_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.acl_file, val);
}
void mgos_config_set_rpc_auth_domain(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.auth_domain, val);
}
void mgos_config_set_rpc_auth_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.auth_file, val);
}
void mgos_config_set_rpc_ws_enable(struct mgos_config *cfg, int         val) {
  cfg->rpc.ws.enable = val;
}
void mgos_config_set_rpc_ws_server_address(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.ws.server_address, val);
}
void mgos_config_set_rpc_ws_reconnect_interval_min(struct mgos_config *cfg, int         val) {
  cfg->rpc.ws.reconnect_interval_min = val;
}
void mgos_config_set_rpc_ws_reconnect_interval_max(struct mgos_config *cfg, int         val) {
  cfg->rpc.ws.reconnect_interval_max = val;
}
void mgos_config_set_rpc_ws_ssl_server_name(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.ws.ssl_server_name, val);
}
void mgos_config_set_rpc_ws_ssl_ca_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.ws.ssl_ca_file, val);
}
void mgos_config_set_rpc_ws_ssl_client_cert_file(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.ws.ssl_client_cert_file, val);
}
void mgos_config_set_rpc_mqtt_enable(struct mgos_config *cfg, int         val) {
  cfg->rpc.mqtt.enable = val;
}
void mgos_config_set_rpc_mqtt_topic(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->rpc.mqtt.topic, val);
}
void mgos_config_set_rpc_uart_uart_no(struct mgos_config *cfg, int         val) {
  cfg->rpc.uart.uart_no = val;
}
void mgos_config_set_rpc_uart_baud_rate(struct mgos_config *cfg, int         val) {
  cfg->rpc.uart.baud_rate = val;
}
void mgos_config_set_rpc_uart_fc_type(struct mgos_config *cfg, int         val) {
  cfg->rpc.uart.fc_type = val;
}
void mgos_config_set_rpc_uart_wait_for_start_frame(struct mgos_config *cfg, int         val) {
  cfg->rpc.uart.wait_for_start_frame = val;
}
void mgos_config_set_mqtt_enable(struct mgos_config *cfg, int         val) {
  cfg->mqtt.enable = val;
}
void mgos_config_set_mqtt_server(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.server, val);
}
void mgos_config_set_mqtt_client_id(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.client_id, val);
}
void mgos_config_set_mqtt_user(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.user, val);
}
void mgos_config_set_mqtt_pass(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.pass, val);
}
void mgos_config_set_mqtt_reconnect_timeout_min(struct mgos_config *cfg, double      val) {
  cfg->mqtt.reconnect_timeout_min = val;
}
void mgos_config_set_mqtt_reconnect_timeout_max(struct mgos_config *cfg, double      val) {
  cfg->mqtt.reconnect_timeout_max = val;
}
void mgos_config_set_mqtt_ssl_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_cert, val);
}
void mgos_config_set_mqtt_ssl_key(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_key, val);
}
void mgos_config_set_mqtt_ssl_ca_cert(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_ca_cert, val);
}
void mgos_config_set_mqtt_ssl_cipher_suites(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_cipher_suites, val);
}
void mgos_config_set_mqtt_ssl_psk_identity(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_psk_identity, val);
}
void mgos_config_set_mqtt_ssl_psk_key(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.ssl_psk_key, val);
}
void mgos_config_set_mqtt_clean_session(struct mgos_config *cfg, int         val) {
  cfg->mqtt.clean_session = val;
}
void mgos_config_set_mqtt_keep_alive(struct mgos_config *cfg, int         val) {
  cfg->mqtt.keep_alive = val;
}
void mgos_config_set_mqtt_will_topic(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.will_topic, val);
}
void mgos_config_set_mqtt_will_message(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mqtt.will_message, val);
}
void mgos_config_set_mqtt_max_qos(struct mgos_config *cfg, int         val) {
  cfg->mqtt.max_qos = val;
}
void mgos_config_set_mqtt_recv_mbuf_limit(struct mgos_config *cfg, int         val) {
  cfg->mqtt.recv_mbuf_limit = val;
}
void mgos_config_set_mcserver_id(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mcserver.id, val);
}
void mgos_config_set_mcserver_topicName(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->mcserver.topicName, val);
}
void mgos_config_set_lightModule_id(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->lightModule.id, val);
}
void mgos_config_set_lightModule_lightSourcePin(struct mgos_config *cfg, int         val) {
  cfg->lightModule.lightSourcePin = val;
}
void mgos_config_set_lightModule_lightSensorPin(struct mgos_config *cfg, int         val) {
  cfg->lightModule.lightSensorPin = val;
}
void mgos_config_set_lightModule_state_lightTurnedOn(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightTurnedOn = val;
}
void mgos_config_set_lightModule_state_mode(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->lightModule.state.mode, val);
}
void mgos_config_set_lightModule_state_lightLvlCheckActive(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightLvlCheckActive = val;
}
void mgos_config_set_lightModule_state_lightLvlCheckInterval(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightLvlCheckInterval = val;
}
void mgos_config_set_lightModule_state_lightLvlCheckLastIntervalCallTimestamp(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightLvlCheckLastIntervalCallTimestamp = val;
}
void mgos_config_set_lightModule_state_lightIntervalsRestTimeTurnedOn(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightIntervalsRestTimeTurnedOn = val;
}
void mgos_config_set_lightModule_state_lightIntervalsCheckingInterval(struct mgos_config *cfg, int         val) {
  cfg->lightModule.state.lightIntervalsCheckingInterval = val;
}
void mgos_config_set_lightModule_state_lightIntervalsArr(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->lightModule.state.lightIntervalsArr, val);
}
void mgos_config_set_irrigationModule_id(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->irrigationModule.id, val);
}
void mgos_config_set_irrigationModule_pumpPin(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.pumpPin = val;
}
void mgos_config_set_irrigationModule_state_irrigationTurnedOn(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTurnedOn = val;
}
void mgos_config_set_irrigationModule_state_mode(struct mgos_config *cfg, const char *val) {
  mgos_conf_set_str(&cfg->irrigationModule.state.mode, val);
}
void mgos_config_set_irrigationModule_state_humidityCheckActive(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckActive = val;
}
void mgos_config_set_irrigationModule_state_humidityCheckInterval(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckInterval = val;
}
void mgos_config_set_irrigationModule_state_humidityCheckLastIntervalCallTimestamp(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckLastIntervalCallTimestamp = val;
}
void mgos_config_set_irrigationModule_state_humidityCheckMinLvl(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckMinLvl = val;
}
void mgos_config_set_irrigationModule_state_humidityCheckAverageLvl(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckAverageLvl = val;
}
void mgos_config_set_irrigationModule_state_humidityCheckMaxLvl(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.humidityCheckMaxLvl = val;
}
void mgos_config_set_irrigationModule_state_irrigationTimerInProgress(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTimerInProgress = val;
}
void mgos_config_set_irrigationModule_state_irrigationTimerEveryXSeconds(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTimerEveryXSeconds = val;
}
void mgos_config_set_irrigationModule_state_irrigationTimerIrrigateYSeconds(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTimerIrrigateYSeconds = val;
}
void mgos_config_set_irrigationModule_state_irrigationTimerLastCallStartTimestamp(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTimerLastCallStartTimestamp = val;
}
void mgos_config_set_irrigationModule_state_irrigationTimerLastCallEndTimestamp(struct mgos_config *cfg, int         val) {
  cfg->irrigationModule.state.irrigationTimerLastCallEndTimestamp = val;
}
/* }}} */
